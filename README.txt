
-------How to compile it------------------

1. Open up the solution in visual studio 2017
2. Set the platform toolset to 141
3. set the build settings to debug and x86
4. build the client, the server, and the auth server



-------How to use it------------------

1.Run the auth server
2.Run the server
3.Run a client

1.The program will prompt each client for a username, and you can type one in an press enter.
2.The program will then prompt you for a room name. You can enter a room name and press enter at this point.
	If the server doesn't yet have a room with that name, it will create one, otherwise it will enter your client into said room.
3.Now you can enter messages into the program. This will send the message to the server which will send the message to all the other clients in the room.
 --NOTE it will not send a message back to the client that sent it as a feature. So you should probably connect at least 2 clients to test.
4.You can switch rooms you are sending a message to by entering: display rooms , then entering the corresponding number of a room
5.You can enter additional rooms by entering: join room ROOMNAME , ie: "join room mango"
6.You can leave a room by entering: leave room ROOMNAME , ie: "leave room mango"
7.To REGISTER a user type in: REGISTER [username] [password] .Type that in the client and press enter.
8.To AUTHENTICATE a user type into the client: AUTHENTICATE [username] [password] .After that press enter.


--NOTE an IP of 127.0.0.1 is assumed for ease of use for this project








