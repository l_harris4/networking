#include "TCPClient.h"
#include <thread>
#include <regex>
#include <algorithm> 
#include <cctype>
#include <locale>
#include "Utils.h"
#include <sstream>
#include <fstream>

using namespace std;

std::vector<std::string> maps;
std::vector<std::string> games;
std::vector<int> players;

void PrintOptions()
{
	cout << "Maps:" << endl;
	for (int i = 0; i < maps.size(); ++i)
	{
		cout << maps[i] << " | ";
	}
	cout << endl << endl << "Games:" << endl;
	for (int i = 0; i < games.size(); ++i)
	{
		cout << games[i] << " | ";
	}
	cout << endl << endl << "Players:" << endl;
	for (int i = 0; i < players.size(); ++i)
	{
		cout << players[i] << " | ";
	}
}

void PrintCommands()
{
	cout << "Commands: " << endl;
	cout << "leave room [room name]" << endl;
	cout << "join room [room name]" << endl;
	cout << "REGISTER [username] [password]" << endl;
	cout << "LOGIN [username] [password]" << endl;
	cout << "make room [map name] [lobby name] [game mode] [max players]" << endl;
	cout << "MAKE ROOM [map name id] [lobby name] [game mode id] [max players id]" << endl;
	cout << "MAKE ROOM ?" << endl;
	cout << "options" << endl;
}

void LoadOptionsFromFile()
{
	std::string filename = "GamesConfig.txt";
	std::string line;
	std::ifstream inFile(filename.c_str());
	std::stringstream ssError;
	bool bAnyErrors = false;

	try {
		while (getline(inFile, line))
		{
			if (line == "")
			{
				continue;
			}
			if (line.find("map:") != std::string::npos) {
				line.replace(0, 5, "");
				maps.push_back(line);
				continue;
			}
			if (line.find("game:") != std::string::npos) {
				line.replace(0, 6, "");
				games.push_back(line);
				continue;
			}
			if (line.find("player:") != std::string::npos) {
				line.replace(0, 8, "");
				players.push_back(atoi(line.c_str()));
				continue;
			}
		}

	}
	catch (...)
	{
	}
}


int main(int argc, char** argv)
{

	TCPClient client;
	thread listenThread;

	string message = "continue";
	cout << "Thank you, now enter a command: " << endl;
	LoadOptionsFromFile();

	cout << "Assume ip of 127.0.0.1 (y/n)" << endl;

	string assumed;
	cin >> assumed;
	if (assumed != "n")
	{
		//attempt to connect to the server
		client.ConnectToServer();
	}
	else
	{
		cout << "IP:";
		std::string tempIp;
		cout << "Please enter IP:";
		cin >> tempIp;
		std::string tempPort;
		cout << "Please enter port:";
		cin >> tempPort;

		//attempt to connect to the server
		client.ConnectToServer(tempIp,tempPort);
	}

	if (client.connected) {
		listenThread = thread(&TCPClient::ListenToServer, client);
		//client.JoinRoom(roomName);

		regex leaveRegex("leave room");
		regex enterRegex("join room");
		regex registerRegex("REGISTER");
		regex authenticateRegex("LOGIN");
		regex makeRoomRegex("make room");
		regex makeRoomRegex2("MAKE ROOM");
		regex optionsRegex("options");
		regex makeroomOptionsRegex("commands");

		while (client.connected)
		{
			getline(cin, message);
			if (client.connected)
			{
				if (regex_search(message, leaveRegex))
				{
					string extractedMessage = message.substr(10, message.size() - 10);
					trim(extractedMessage);
					client.LeaveRoom(extractedMessage);
				}
				else if (regex_search(message, enterRegex))
				{
					string extractedMessage = message.substr(9, message.size() - 9);
					trim(extractedMessage);
					client.JoinRoom(extractedMessage);
				}
				else if (regex_search(message, optionsRegex))
				{
					PrintOptions();
				}
				else if (regex_search(message, makeroomOptionsRegex))
				{
					PrintCommands();
				}
				else if (message == "display rooms")
				{
					if (client.roomNames.size() > 1)
					{
						cout << "[Press the corresponding number to make a room the current room]" << endl;
						int size = client.roomNames.size();
						for (int roomIndex = 0; roomIndex < size; roomIndex++)
						{
							cout << "[" << roomIndex << "]" << client.roomNames[roomIndex] << " ";
						}
						cout << endl;
						string response;
						cin >> response;
						stringstream conversionStream;
						conversionStream.clear();
						conversionStream.str("");
						conversionStream.str(response);
						int responseNumber = 0;
						conversionStream >> responseNumber;
						if ((unsigned)responseNumber < client.roomNames.size())
						{
							client.roomNameIndex = responseNumber;
							cout << "Current room is now: " << client.roomNames[client.roomNameIndex] << endl;
						}
						else
						{
							cout << "Not valid input: " << endl;
						}

					}
					else if (client.roomNames.size() == 1)
					{
						cout << "Only one room currently: " << client.roomNames.front() << endl;
					}
					else
					{
						cout << "You are in zero rooms currently " << endl;
					}

				}
				else if (regex_search(message, registerRegex))
				{
					string extractedMessage = message.substr(8, message.size() - 8);
					trim(extractedMessage);
					string tempEmail;
					string tempPassword;
					tempPassword = extractedMessage.substr(extractedMessage.find_first_of(' ') + 1);
					tempEmail = extractedMessage.substr(0, extractedMessage.find_first_of(' '));
					trim(tempPassword);
					trim(tempEmail);

					client.Register(tempEmail, tempPassword);
				}
				else if (regex_search(message, authenticateRegex))
				{
					string extractedMessage = message.substr(5, message.size() - 5);
					trim(extractedMessage);
					string tempEmail;
					string tempPassword;
					tempPassword = extractedMessage.substr(extractedMessage.find_first_of(' ') + 1);
					tempEmail = extractedMessage.substr(0, extractedMessage.find_first_of(' '));
					trim(tempPassword);
					trim(tempEmail);

					client.Authenticate(tempEmail, tempPassword);
				}
				else if (regex_search(message, makeRoomRegex2))
				{


					string extractedMessage = message.substr(9, message.size() - 9);
					trim(extractedMessage);
					//string mapName;
					int mapNameIndex;
					string roomName;
					int gameModeIndex;
					//string gameMode;
					int player;
					int offset = 0;
					int length = 0;
					int lengthSoFar = 0;
					length = extractedMessage.find(' ');
					lengthSoFar = extractedMessage.find(' ');
					mapNameIndex = atoi(extractedMessage.substr(0, length).c_str());
					offset += to_string(mapNameIndex).size();
					length = lengthSoFar;
					lengthSoFar = extractedMessage.find(' ', lengthSoFar + 1);
					length = lengthSoFar - length;

					roomName = extractedMessage.substr(offset, length);
					offset += roomName.size();
					length = lengthSoFar;
					lengthSoFar = extractedMessage.find(' ', lengthSoFar + 1);
					length = lengthSoFar - length;
					gameModeIndex = atoi(extractedMessage.substr(offset, length).c_str());
					offset += to_string(gameModeIndex).size();

					length = lengthSoFar;
					lengthSoFar = extractedMessage.find(' ', lengthSoFar + 1);
					length = lengthSoFar - length;
					player = atoi(extractedMessage.substr(offset, length).c_str());
					trim(roomName);

					client.MakeRoom(maps[mapNameIndex], roomName, games[gameModeIndex], players[player]);
				}
				else if (regex_search(message, makeRoomRegex))
				{


					string extractedMessage = message.substr(9, message.size() - 9);
					trim(extractedMessage);
					string mapName;
					string roomName;
					string gameMode;
					int player;
					int offset = 0;
					int length = 0;
					int lengthSoFar = 0;
					length = extractedMessage.find(' ');
					lengthSoFar = extractedMessage.find(' ');
					mapName = extractedMessage.substr(0, length);
					offset += mapName.size();
					length = lengthSoFar;
					lengthSoFar = extractedMessage.find(' ', lengthSoFar + 1);
					length = lengthSoFar - length;

					roomName = extractedMessage.substr(offset, length);
					offset += roomName.size();
					length = lengthSoFar;
					lengthSoFar = extractedMessage.find(' ', lengthSoFar + 1);
					length = lengthSoFar - length;
					gameMode = extractedMessage.substr(offset, length);
					offset += gameMode.size();
					length = lengthSoFar;
					lengthSoFar = extractedMessage.find(' ', lengthSoFar + 1);
					length = lengthSoFar - length;
					player = atoi(extractedMessage.substr(offset, length).c_str());
					trim(mapName);
					trim(roomName);
					trim(gameMode);

					client.MakeRoom(mapName, roomName, gameMode, player);
				}

			}
		}

		//wait for the listen thread to stop
		listenThread.join();
	}
	else
	{
		cout << "Failed to connect to server." << endl;
	}
	system("pause");
	exit(0);
	return 0;
}