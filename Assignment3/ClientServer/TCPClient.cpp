#include "TCPClient.h"
#include <iostream>
#include <stdlib.h>

void TCPClient::ClearScreen()
{
	system("cls");
	std::cout << mostRecentMessage << std::endl;
}

//constructor
TCPClient::TCPClient()
{
	roomNameIndex = 0;
	messageBuffer = new Buffer(DEFAULT_BUFFER_LENGTH);

	std::cout << "Client running" << std::endl;
	connected = true;

	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &mWsaData);
	if (iResult != 0) {
		printf("WSAStartup failed: %d\n", iResult);
		connected = false;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	//assumed ip of home for this project
	mServerIP = "127.0.0.1";



}

//destructor
TCPClient::~TCPClient()
{
	delete messageBuffer;
	iResult = shutdown(mConnectSocket, SD_SEND);
	if (iResult == SOCKET_ERROR) {
		printf("shutdown() failed with error: %d\n", iResult);
		closesocket(mConnectSocket);
		WSACleanup();
		connected = false;
	}

	closesocket(mConnectSocket);
	WSACleanup();
}

//Name: ConnectToServer
//Purpose: to connect the client to the server
//Accepts: nothing
//Returns: nothing
void TCPClient::ConnectToServer()
{
	iResult = getaddrinfo(mServerIP.c_str(), DEFAULT_PORT, &hints, &result);
	if (iResult != 0) {
		printf("getaddrinfo failed with error: %d\n", iResult);
		WSACleanup();
		connected = false;
	}
	for (ptr = result; ptr != NULL; ptr = ptr->ai_next) {
		mConnectSocket = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);
		if (mConnectSocket == INVALID_SOCKET) {
			printf("socket() failed with error: %d\n", iResult);
			WSACleanup();
			connected = false;
		}

		if (connected) {
			iResult = connect(mConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
			if (iResult == SOCKET_ERROR) {
				closesocket(mConnectSocket);
				mConnectSocket = INVALID_SOCKET;
				continue;
			}
		}

		break;
	}

	freeaddrinfo(result);

	if (mConnectSocket == INVALID_SOCKET) {
		printf("Unable to connect to server");
		WSACleanup();
		connected = false;
	}
	if (connected)
	{
		std::cout << "Connected to server" << std::endl;
	}
}

void TCPClient::ConnectToServer(std::string ip, std::string port)
{
	mServerIP = ip;
	iResult = getaddrinfo(mServerIP.c_str(), port.c_str(), &hints, &result);
	if (iResult != 0) {
		printf("getaddrinfo failed with error: %d\n", iResult);
		WSACleanup();
		connected = false;
	}
	for (ptr = result; ptr != NULL; ptr = ptr->ai_next) {
		mConnectSocket = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);
		if (mConnectSocket == INVALID_SOCKET) {
			printf("socket() failed with error: %d\n", iResult);
			WSACleanup();
			connected = false;
		}

		if (connected) {
			iResult = connect(mConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
			if (iResult == SOCKET_ERROR) {
				closesocket(mConnectSocket);
				mConnectSocket = INVALID_SOCKET;
				continue;
			}
		}

		break;
	}

	freeaddrinfo(result);

	if (mConnectSocket == INVALID_SOCKET) {
		printf("Unable to connect to server");
		WSACleanup();
		connected = false;
	}
	if (connected)
	{
		std::cout << "Connected to server" << std::endl;
	}
}



//Name: JoinRoom
//Purpose: to join the client to a room on the server
//Accepts: a string representing the room name that the client wants to join
//Returns: nothing
void TCPClient::JoinRoom(std::string roomName)
{

	//send username and room you want to join

	messageBuffer->SetWriteIndex(0);
	//serialize the total length
	int totalLength = 4 + roomName.size();
	messageBuffer->WriteInt32BE(totalLength);
	//serialize the message id
	messageBuffer->WriteInt32BE(MessageType::JOINROOM);

	//serialize room name length
	messageBuffer->WriteInt32BE(roomName.size());
	//serialize room name
	messageBuffer->WriteString(roomName);

	//actually sending the data
	iResult = send(mConnectSocket, messageBuffer->GetBufferData(), sizeof(uint32_t) * 2, 0);
	iResult = send(mConnectSocket, messageBuffer->GetBufferData() + 8, totalLength, 0);
	if (iResult == SOCKET_ERROR) {
		printf("socket() failed with error: %d\n", iResult);
		closesocket(mConnectSocket);
		WSACleanup();
		connected = false;
	}
	//InsertRoomName(roomName);
}

//Name: LeaveRoom
//Purpose: to take this client out of a room on the server
//Accepts: a string representing the room name that the client wants to leave
//Returns: nothing
void TCPClient::LeaveRoom(std::string roomName)
{//connectToServer();
	//send username and room you want to join

	messageBuffer->SetWriteIndex(0);
	//serialize the total length
	int totalLength = 8 + roomName.size();
	messageBuffer->WriteInt32BE(totalLength);
	//serialize the message id
	messageBuffer->WriteInt32BE(MessageType::LEAVEROOM);

	//serialize room name length
	messageBuffer->WriteInt32BE(roomName.size());
	//serialize room name
	messageBuffer->WriteString(roomName);

	//actually sending the data
	iResult = send(mConnectSocket, messageBuffer->GetBufferData(), sizeof(uint32_t) * 2, 0);
	iResult = send(mConnectSocket, messageBuffer->GetBufferData() + 8, totalLength, 0);
	if (iResult == SOCKET_ERROR) {
		printf("socket() failed with error: %d\n", iResult);
		closesocket(mConnectSocket);
		WSACleanup();
		connected = false;
		std::cout << "Lost connection to server" << std::endl;
	}
}

//Name: SendMessageToServer
//Purpose: to send a string message to the server
//Accepts: a string representing the room name that the client wants to leave,
//         and the message itself
//Returns: nothing
void TCPClient::SendMessageToServer(std::string message, std::string roomName)
{
}

//Name: ListenToServer
//Purpose: to receive messages from the server
//Accepts: nothing
//Returns: nothing
void TCPClient::ListenToServer()
{
	do {
		//take in the header //getting packet length and message id
		iResult = recv(mConnectSocket, messageBuffer->GetBufferData(), 8, 0);

		if (iResult == SOCKET_ERROR) {
			printf("recv failed with error: %d\n", WSAGetLastError());
			closesocket(mConnectSocket);
			WSACleanup();
			connected = false;
			std::cout << "Lost connection to server" << std::endl;
		}

		if (iResult == 0) {
			closesocket(mConnectSocket);
			WSACleanup();
			connected = false;
			std::cout << "Lost connection to server" << std::endl;
		}

		if (connected)
		{
			messageBuffer->SetReadIndex(0);

			int packetLength = messageBuffer->ReadInt32BE();
			int message_id = messageBuffer->ReadInt32BE();

			if (packetLength > messageBuffer->GetBufferSize()) {
				messageBuffer->ResizeBuffer(packetLength);
			}

			//if incoming packet is larger than buffer, increase buffer size
			if (packetLength > messageBuffer->GetBufferSize()) {
				messageBuffer->ResizeBuffer(packetLength);
			}

			switch (message_id)
			{
			case MessageType::MESSAGE:
			{
				recv(mConnectSocket, messageBuffer->GetBufferData(), 4, 0);
				messageBuffer->SetReadIndex(0);
				int strLength = messageBuffer->ReadInt32BE();
				recv(mConnectSocket, messageBuffer->GetBufferData(), strLength, 0);
				messageBuffer->SetReadIndex(0);
				std::string line = messageBuffer->ReadString(strLength);
				std::cout << line << std::endl;
				mostRecentMessage = line;
			}
			break;
			case MessageType::GAMEROOMDUMP:
			{
				ClearScreen();
				if (packetLength == 0)
				{
					std::cout << "No Games Presently" << std::endl;
				}
				else
				{
					std::cout << "ALL GAMES" << std::endl;
					std::cout << "ID        MAP       ROOM      GAME      PLAYERS   HOST" << std::endl;
					for (int i = 0; i < packetLength; ++i)
					{
						recv(mConnectSocket, messageBuffer->GetBufferData(), 4, 0);
						messageBuffer->SetReadIndex(0);
						int strLength = messageBuffer->ReadInt32BE();
						recv(mConnectSocket, messageBuffer->GetBufferData(), strLength, 0);
						messageBuffer->SetReadIndex(0);
						std::string line = messageBuffer->ReadString(strLength);
						std::cout << line << std::endl;
					}
				}
			}
			break;
			case MessageType::SINGLEROOM:
			{
				ClearScreen();
				recv(mConnectSocket, messageBuffer->GetBufferData(), 4, 0);
				messageBuffer->SetReadIndex(0);
				int strLength = messageBuffer->ReadInt32BE();
				recv(mConnectSocket, messageBuffer->GetBufferData(), strLength, 0);
				messageBuffer->SetReadIndex(0);
				std::string line = messageBuffer->ReadString(strLength);
				std::cout << "CURRENT GAME" << std::endl;
				std::cout << "ID        MAP       ROOM      GAME      PLAYERS   HOST" << std::endl;
				std::cout << line << std::endl << std::endl << std::endl << std::endl << std::endl << std::endl << std::endl;
			}
			break;
			case MessageType::REGISTERSUCCESS:
			{
				std::cout << "Registration successful" << std::endl;
			}
			break;
			case MessageType::REGISTERFAILUREACCOUNTEXISTS:
			{
				std::cout << "Account with that email already exists" << std::endl;
			}
			break;
			case MessageType::REGISTERFAILURENOCONNECTIONTOSERVER:
			{
				std::cout << "Internal server error" << std::endl;
			}
			break;
			case MessageType::AUTHENTICATESUCCESS:
			{
				//might have to do another recv here using the packetLength
				iResult = recv(mConnectSocket, messageBuffer->GetBufferData(), packetLength, 0);
				if (iResult > 0)
				{
					messageBuffer->SetReadIndex(0);
					int dateLength = messageBuffer->ReadInt32BE();
					std::string date = messageBuffer->ReadString(dateLength);

					std::cout << "Account is valid" << std::endl;
					std::cout << "Created: " << date << std::endl;
				}
			}
			break;
			case MessageType::AUTHENTICATEFAILUREWRONGPASSWORD:
			{
				std::cout << "Invalid password" << std::endl;
			}
			break;
			case MessageType::AUTHENTICATEFAILURENOCONNECTIONSERVER:
			{;
				std::cout << "Internal server error" << std::endl;
			}
			break;
			case MessageType::AUTHENTICATEFAILURENOACCOUNT:
			{
				std::cout << "Account does not exist" << std::endl;
			}
			break;
			}
		}

	} while (iResult > 0 && connected);
}

//Name: InsertRoomName
//Purpose: to add a room name to the local copy of what rooms the client is in
//Accepts: the name of the room to join
//Returns: nothing
void TCPClient::InsertRoomName(std::string roomName)
{
	roomNames.push_back(roomName);
}

//Name: DeleteRoomName
//Purpose: to remove a room name from the local copy of what rooms the client is in
//Accepts: the name of the room to leave
//Returns: nothing
void TCPClient::DeleteRoomName(std::string roomName)
{
	roomNames.erase(std::remove(roomNames.begin(), roomNames.end(), roomName), roomNames.end());
	if (roomNameIndex >= roomNames.size())
	{
		roomNameIndex = 0;
	}
}

//Name: Register
//Purpose: to register a user in the database
//Accepts: a string being the email and a string being the password of a user
//Returns: nothing
void TCPClient::Register(std::string email, std::string password)
{
	messageBuffer->SetWriteIndex(0);
	//serialize the total length
	int totalLength = 12 + email.size() + password.size();
	messageBuffer->WriteInt32BE(totalLength);
	//serialize the message id
	messageBuffer->WriteInt32BE(MessageType::REGISTER);

	//serialize room name length
	messageBuffer->WriteInt32BE(email.size());
	//serialize room name
	messageBuffer->WriteString(email);
	//serialize the username length
	messageBuffer->WriteInt32BE(password.size());
	//serialize the username
	messageBuffer->WriteString(password);
	messageBuffer->WriteInt32BE(-1);

	//actually sending the data
	iResult = send(mConnectSocket, messageBuffer->GetBufferData(), sizeof(uint32_t) * 2, 0);
	iResult = send(mConnectSocket, messageBuffer->GetBufferData() + 8, totalLength, 0);
	if (iResult == SOCKET_ERROR) {
		printf("socket() failed with error: %d\n", iResult);
		closesocket(mConnectSocket);
		WSACleanup();
		connected = false;
	}
}


//Name: Authenticate
//Purpose: to test if a user is in the database with a specific password
//Accepts: a string being the email and a string being the password of a user
//Returns: nothing
void TCPClient::Authenticate(std::string email, std::string password)
{

	//NOT USING GOOGLE
	//send username and room you want to join

	messageBuffer->SetWriteIndex(0);
	//serialize the total length
	int totalLength = 12 + email.size() + password.size();
	messageBuffer->WriteInt32BE(totalLength);
	//serialize the message id
	messageBuffer->WriteInt32BE(MessageType::AUTHENTICATE);

	//serialize room name length
	messageBuffer->WriteInt32BE(email.size());
	//serialize room name
	messageBuffer->WriteString(email);
	//serialize the username length
	messageBuffer->WriteInt32BE(password.size());
	//serialize the username
	messageBuffer->WriteString(password);
	messageBuffer->WriteInt32BE(-1);

	//actually sending the data
	iResult = send(mConnectSocket, messageBuffer->GetBufferData(), sizeof(uint32_t) * 2, 0);
	iResult = send(mConnectSocket, messageBuffer->GetBufferData() + 8, totalLength, 0);
	if (iResult == SOCKET_ERROR) {
		printf("socket() failed with error: %d\n", iResult);
		closesocket(mConnectSocket);
		WSACleanup();
		connected = false;
	}
}

void TCPClient::MakeRoom(std::string mapName, std::string roomName, std::string gameMode, int player)
{
	//NOT USING GOOGLE
	//send username and room you want to join

	messageBuffer->SetWriteIndex(0);
	//serialize the total length
	int totalLength = 20 + mapName.size() + roomName.size() + gameMode.size();
	messageBuffer->WriteInt32BE(totalLength);
	//serialize the message id
	messageBuffer->WriteInt32BE(MessageType::MAKEROOM);

	//serialize room name length
	messageBuffer->WriteInt32BE(mapName.size());
	//serialize room name
	messageBuffer->WriteString(mapName);
	//serialize the username length
	messageBuffer->WriteInt32BE(roomName.size());
	//serialize the username
	messageBuffer->WriteString(roomName);

	//serialize the username length
	messageBuffer->WriteInt32BE(gameMode.size());
	//serialize the username
	messageBuffer->WriteString(gameMode);

	messageBuffer->WriteInt32BE(player);

	//actually sending the data
	iResult = send(mConnectSocket, messageBuffer->GetBufferData(), sizeof(uint32_t) * 2, 0);
	iResult = send(mConnectSocket, messageBuffer->GetBufferData() + 8, totalLength, 0);
	if (iResult == SOCKET_ERROR) {
		printf("socket() failed with error: %d\n", iResult);
		closesocket(mConnectSocket);
		WSACleanup();
		connected = false;
	}
}

