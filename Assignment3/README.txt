
-------How to compile it------------------

1. Open up the solution in visual studio 2017
2. Set the platform toolset to 141
3. set the build settings to debug and x86
4. build the client, the server, and the auth server



-------How to use it------------------

For changing which db the TCP server connects to, along with what username and what password are used,
change line 102 of TCPServerAuth.cpp accordingly.

(When starting things up, start them through visual studio debug mode)
1.Run the auth server
2.Run the server
3.Run a client(s)

1. Firstly each client will be asked if they want to use the default ip of
127.0.0.1. If you enter "n" at this stage you will have to provide and ip
and port. You will be prompted seperately for them. The port for the server
by default 5000.

2.If you want the program to show you commands then simply type "commands" 
and enter.

3. The client can now register or login. Using "REGISTER [username] [password]"
and "LOGIN [username] [password]" respectively.

4. Once a client has logged in they will be presented with a list of all 
active games. At this point they can join a game or create a game.

5. For creating a game they have the choice of making up their own map, 
game mode, and player number, or they can use the ones provided by an external
file. For viewing the ones provided enter "options". For creating a room
using made up values enter 
"make room [map name] [lobby name] [game mode] [max players]"
for creating a room/lobby using provided information enter
"MAKE ROOM [map name id] [lobby name] [game mode id] [max players id]".
The client will always be expected to make up a lobby name.

6. For joining a room simply enter "join room [room name]".

7. For leaving a room simply enter "leave room [room name]".

NOTES:











