#include "TCPServerAuth.h"
#include <stdio.h>      /* printf, scanf, puts, NULL */
#include <stdlib.h>     /* srand, rand */
#include <time.h> 




static bool deleteAll(SOCKET * theElement) { delete theElement; return true; }

//Name: PlainTextToHash
//Purpose: to hash plaintext
//Accepts: a const char* that is the text to be converted
//Returns: a string being the hashed text
std::string TCPServerAuth::PlainTextToHash(const char* plainText)
{
	unsigned char digest[SHA256_DIGEST_LENGTH];
	SHA256_Update(&ctx, plainText, strlen(plainText));
	SHA256_Final(digest, &ctx);

	char hash[SHA256_DIGEST_LENGTH * 2 + 1];
	for (int i = 0; i < SHA256_DIGEST_LENGTH; i++)
	{
		sprintf(&(hash[i * 2]), "%02x", (unsigned int)(digest[i]));
	}

	SHA256_Init(&ctx);

	return hash;
}

//Name: RandomSalt
//Purpose: to create a random string used for salting
//Accepts: a const int used as the length of the generated string
//Returns: a generated random string
std::string RandomSalt(const int len) {
	std::string returnString = "";
	static const char alphanum[] =
		"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	int size = (sizeof(alphanum) - 1);

	srand((unsigned)time(NULL));

	for (int i = 0; i < len; ++i) {
		int random = rand() % size;
		char add = alphanum[random];
		returnString += add;
	}

	return returnString;
}

//constructor
TCPServerAuth::TCPServerAuth()
{
	

	messageBuffer = new Buffer(DEFAULT_BUFFER_LENGTH);
	std::cout << "Auth Server running" << std::endl;
	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;
	connected = true;


	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (connected && iResult != 0) {
		printf("WSAStartup failed: %d\n", iResult);
		connected = false;
	}

	// Socket()
	ListenSocket = socket(AF_INET, SOCK_STREAM, 0);
	if (connected && ListenSocket == INVALID_SOCKET) {
		printf("socket() failed with error %d\n", WSAGetLastError());
		WSACleanup();
		connected = false;
	}

	// Bind()
	iResult = getaddrinfo(NULL, DEFAULT_PORT, &hints, &result);
	iResult = bind(ListenSocket, result->ai_addr, (int)result->ai_addrlen);
	if (connected && iResult == SOCKET_ERROR) {
		printf("bind() failed with error: %d\n", WSAGetLastError());
		freeaddrinfo(result);
		closesocket(ListenSocket);
		WSACleanup();
		connected = false;
	}

	// Listen()
	if (connected && listen(ListenSocket, 5)) {
		printf("listen() failed with error: %d\n", WSAGetLastError());
		closesocket(ListenSocket);
		WSACleanup();
		connected = false;
	}

	//connect to the db
	dbConnected = !dbHelper.ConnectToDatabase("127.0.0.1:3306", "root", "root", "authentication");

	SHA256_Init(&ctx);

}

//destructor
TCPServerAuth::~TCPServerAuth()
{
	for (unsigned threadIndex = 0; threadIndex < threads.size(); ++threadIndex)
	{
		threads[threadIndex]->join();
	}
	delete messageBuffer;
	for (unsigned clientIndex = 0; clientIndex < clients.size(); clientIndex++)
	{
		closesocket(clients[clientIndex].socket);
	}
	closesocket(ListenSocket);
	WSACleanup();
}

//Name: AcceptClient
//Purpose: accepts a new client a starts a thread to listen to that client
//Accepts: nothing
//Returns: nothing
void TCPServerAuth::AcceptClient()
{
	while (active)
	{
		SOCKET ClientSocket;
		// Accept()
		ClientSocket = accept(ListenSocket, NULL, NULL);
		if (ClientSocket == INVALID_SOCKET)
		{
			printf("accept() failed with error: %d\n", WSAGetLastError());
			closesocket(ListenSocket);
			//WSACleanup();
		}
		else
		{
			std::cout << "Client Connected" << std::endl;
			threads.push_back(new std::thread(&TCPServerAuth::ListenToClient, this, ClientSocket));
		}
	}
}

//Name: ListenToClient
//Purpose: to listen for messages from a specific 
//Accepts: a socket
//Returns: nothing
void TCPServerAuth::ListenToClient(SOCKET clientSocket)
{
	Client tempClient;
	tempClient.socket = clientSocket;

	do {
		//take in the header //getting packet length and message id
		iResult = recv(tempClient.socket, messageBuffer->GetBufferData(), sizeof(uint32_t) * 2, 0);

		if (iResult > 0)
		{
			messageBuffer->SetReadIndex(0);

			int packetLength = messageBuffer->ReadInt32BE();
			int message_id = messageBuffer->ReadInt32BE();

			if (packetLength > messageBuffer->GetBufferSize()) {
				messageBuffer->ResizeBuffer(packetLength);
			}

			switch (message_id)
			{
			case MessageType::REGISTER:
			{
				//might have to do another recv here using the packetLength
				iResult = recv(tempClient.socket, messageBuffer->GetBufferData(), packetLength, 0);
				if (iResult > 0)
				{
					messageBuffer->SetReadIndex(0);
					//get the room name
					std::string email;
					std::string password;
					int emailLength = messageBuffer->ReadInt32BE();
					email = messageBuffer->ReadString(emailLength);

					//get the username
					int passwordLength = messageBuffer->ReadInt32BE();
					password = messageBuffer->ReadString(passwordLength);
					int requestId = messageBuffer->ReadInt32BE();

					sql::ResultSet* resultSet = dbHelper.ExecuteQuery("SELECT * from accounts where username = '" + email + "' limit 1");
					int rowsResult = resultSet->rowsCount();
					try {
						if (rowsResult != 1)
						{
							//dbHelper.ExecuteUpdate("INSERT INTO acounts (last_login_time, creation_date) VALUES (now(), now());");
							//sql::ResultSet* resultSet = dbHelper.ExecuteQuery("SELECT * from user");
							//sql::ResultSet* resultSet = dbHelper.ExecuteQuery("SELECT * from user order by creation_date desc limit 1");
							//int rowsResult = resultSet->rowsCount();
							//sql::ResultSetMetaData *res_meta = resultSet->getMetaData();
							//resultSet->next();
							//int userId = resultSet->getInt("id");
							std::string salt = RandomSalt(4);
							password = salt + password;
							std::string hashedPassword = PlainTextToHash(password.c_str());
							int rows = dbHelper.ExecuteUpdate("INSERT INTO accounts (username, salt, password, last_login) VALUES ('" + email + "','" + salt+ "','"+ hashedPassword + "',now());");


							messageBuffer->SetWriteIndex(0);
							//serialize the total length
							int totalLength = 4;
							messageBuffer->WriteInt32BE(totalLength);
							//serialize the message id
							messageBuffer->WriteInt32BE(MessageType::REGISTERSUCCESS);


							messageBuffer->WriteInt32BE(requestId);

							iResult = send(clientSocket, messageBuffer->GetBufferData(), 8, 0);
							iResult = send(clientSocket, messageBuffer->GetBufferData() + 8, 4, 0);
						}
						//otherwise send creation failure
						else
						{

							messageBuffer->SetWriteIndex(0);
							//serialize the total lengtha
							int totalLength = 4;
							messageBuffer->WriteInt32BE(totalLength);
							//serialize the message id
							messageBuffer->WriteInt32BE(MessageType::REGISTERFAILUREACCOUNTEXISTS);
							messageBuffer->WriteInt32BE(requestId);

							iResult = send(clientSocket, messageBuffer->GetBufferData(), 8, 0);
							iResult = send(clientSocket, messageBuffer->GetBufferData()+8, 4, 0);
						}
					}
					catch (...)
					{
						//if some other error occurred along the way, send that to the client
						messageBuffer->SetWriteIndex(0);

						//serialize the total length
						int totalLength = 4;
						messageBuffer->WriteInt32BE(totalLength);
						//serialize the message id
						messageBuffer->WriteInt32BE(MessageType::REGISTERFAILURENOCONNECTIONTOSERVER);
						messageBuffer->WriteInt32BE(requestId);

						iResult = send(clientSocket, messageBuffer->GetBufferData(), 8, 0);
						iResult = send(clientSocket, messageBuffer->GetBufferData() + 8, 4, 0);
					}


				}
			}
			break;
			case MessageType::AUTHENTICATE:
			{
				//might have to do another recv here using the packetLength
				iResult = recv(tempClient.socket, messageBuffer->GetBufferData(), packetLength, 0);
				if (iResult > 0)
				{
					messageBuffer->SetReadIndex(0);
					//get the room name
					std::string email;
					std::string password;
					int emailLength = messageBuffer->ReadInt32BE();
					email = messageBuffer->ReadString(emailLength);

					//get the username
					int passwordLength = messageBuffer->ReadInt32BE();
					password = messageBuffer->ReadString(passwordLength);
					int requestID = messageBuffer->ReadInt32BE();

					sql::ResultSet* resultSetTest = dbHelper.ExecuteQuery("SELECT * from accounts where username = '" + email + "' limit 1;");

					if (resultSetTest->rowsCount() == 1)
					{
						resultSetTest->next();
						std::string salt = resultSetTest->getString("salt");
						password = salt + password;
						std::string hashedPassword = PlainTextToHash(password.c_str());
						password = hashedPassword;
					}

					sql::ResultSet* resultSet = dbHelper.ExecuteQuery("SELECT * FROM accounts where username = '" + email + "' AND password = '" + password + "' limit 1;");
					

					//if you found stuff here, send authentication success to client
					try {
						if (resultSet->rowsCount() == 1)
						{
							resultSet->next();
							//update the last login time
							dbHelper.Execute("UPDATE accounts SET last_login = now() where username = '" + email + "';");

							messageBuffer->SetWriteIndex(0);

							int totalLength = 4;
							messageBuffer->WriteInt32BE(totalLength);
							messageBuffer->WriteInt32BE(MessageType::AUTHENTICATESUCCESS);

							messageBuffer->WriteInt32BE(requestID);

							iResult = send(clientSocket, messageBuffer->GetBufferData(), 8, 0);
							iResult = send(clientSocket, messageBuffer->GetBufferData() + 8, totalLength, 0);
						}
						//if not, then send authentication failure to client
						else
						{

							sql::ResultSet* resultSetEmail = dbHelper.ExecuteQuery("SELECT * from accounts where username = '" + email + "' limit 1;");
							bool emailTest = resultSetEmail->rowsCount() == 1;

							messageBuffer->SetWriteIndex(0);
							int totalLength = 4;
							messageBuffer->WriteInt32BE(totalLength);

							//if the email does exist in our db
							if (emailTest)
							{
								messageBuffer->WriteInt32BE(MessageType::AUTHENTICATEFAILUREWRONGPASSWORD);
							}
							else
							{
								messageBuffer->WriteInt32BE(MessageType::AUTHENTICATEFAILURENOACCOUNT);
							}

							messageBuffer->WriteInt32BE(requestID);

							iResult = send(clientSocket, messageBuffer->GetBufferData(), 8, 0);
							iResult = send(clientSocket, messageBuffer->GetBufferData() + 8, totalLength, 0);
						}
					}
					catch (...)
					{
						messageBuffer->SetWriteIndex(0);

						int totalLength = 4;
						messageBuffer->WriteInt32BE(totalLength);
						messageBuffer->WriteInt32BE(MessageType::AUTHENTICATEFAILURENOCONNECTIONSERVER);

						messageBuffer->WriteInt32BE(requestID);

						iResult = send(clientSocket, messageBuffer->GetBufferData(), 8, 0);
						iResult = send(clientSocket, messageBuffer->GetBufferData() + 8, totalLength, 0);
					}
				}
			}
			break;

			}
		}


		if (iResult == SOCKET_ERROR) {
			printf("recv failed with error: %d\n", WSAGetLastError());
			closesocket(tempClient.socket);
		}

		if (iResult > 0) {
			printf("Bytes Received: %d\n", iResult);
		}

		if (iResult == 0) {
			printf("Connection closing...\n");
		}

		std::cout << "Data recieved" << std::endl;

	} while (iResult > 0 && active);
}

