#ifndef DATABASE_CONNECTOR_HG
#define DATABASE_CONNECTOR_HG

#include <cppconn\driver.h>
#include <cppconn\exception.h>
#include <cppconn\resultset.h>
#include <cppconn\statement.h>
#include <cppconn\prepared_statement.h>
#include <cppconn\resultset_metadata.h>

#include <string>

class DatabaseConnector
{
public:
	//ctor
	DatabaseConnector();
	//dtor
	~DatabaseConnector();

	//methods
	bool ConnectToDatabase(const std::string &server, const std::string &username, const std::string &password, const std::string &schema);
	bool Execute(const std::string &message);
	sql::ResultSet* ExecuteQuery(const std::string &message);
	int ExecuteUpdate(const std::string &message);

private:
	//data members
	sql::Driver* driver;
	sql::Connection* con;
	sql::Statement* stmt;
	sql::PreparedStatement* pstmt;
};

#endif

