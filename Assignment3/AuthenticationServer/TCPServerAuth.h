#ifndef  _TCPSERVERAUTH_HG_
#define _TCPSERVERAUTH_HG_
#define UNICODE
#define WIN32_LEAN_AND_MEAN

#include <Windows.h>
#include <WinSock2.h>
#include <WS2tcpip.h>

#include <iostream>
#include <string>
#include <vector>
#include <thread>

#include "../Server/Buffer.h"
#include "../Server/Client.h"
#include "DatabaseConnector.h"
#include <string>
#include <fstream>
#include <iostream>
#include <string>
#include <stdio.h>

#include <openssl\conf.h>
#include <openssl\evp.h>
#include <openssl\err.h>
#include <openssl\sha.h>

#pragma comment(lib, "Ws2_32.lib")

#define DEFAULT_PORT "4000"
#define DEFAULT_BUFFER_LENGTH 512



class TCPServerAuth
{
public:
	//ctor
	TCPServerAuth();
	//dtor
	~TCPServerAuth();
private:
	//data members
	WSADATA wsaData;
	int iResult;
	SOCKET ListenSocket;
	std::vector<std::thread*> threads;
	std::vector<Client> clients;
	struct addrinfo* result = 0;
	struct addrinfo hints;
	bool connected;
	Buffer* messageBuffer;
	DatabaseConnector dbHelper;
	bool dbConnected;
	SHA256_CTX ctx;
public:
	bool active = true;
	//methods
	void ListenToClient(SOCKET clientSocket);
	std::string PlainTextToHash(const char* plainText);
	void AcceptClient();
};

enum MessageType
{
	MESSAGE = 0,
	JOINROOM,
	LEAVEROOM,
	REGISTER,
	REGISTERSUCCESS,
	REGISTERFAILUREACCOUNTEXISTS,
	REGISTERFAILURENOCONNECTIONTOSERVER,
	AUTHENTICATE,
	AUTHENTICATESUCCESS,
	AUTHENTICATEFAILUREWRONGPASSWORD,
	AUTHENTICATEFAILURENOCONNECTIONSERVER,
	AUTHENTICATEFAILURENOACCOUNT
};

#endif // ! _TCPSERVERAUTH_HG_

