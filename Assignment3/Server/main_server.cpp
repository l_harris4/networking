#include "TCPServer.h"

using namespace std;

int main() {
	//create the server
	TCPServer server;

	//let the server accept clients
	thread acceptClientsThread (&TCPServer::AcceptClient,server);
	string test = "";

	while (test != "q")
	{
		cin >> test;
		if (test == "q")
		{
			acceptClientsThread.join();
			server.active = false;

		}
	}
	system("pause");
}