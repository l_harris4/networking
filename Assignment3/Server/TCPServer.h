#ifndef  _TCPSERVER_HG_
#define _TCPSERVER_HG_
#define WIN32_LEAN_AND_MEAN

#include <Windows.h>
#include <WinSock2.h>
#include <WS2tcpip.h>

#include <iostream>
#include <string>
#include <vector>
#include <thread>

#include "Buffer.h"
#include "Room.h"

//#include "../shared/chatapp.pb.h"

#pragma comment(lib, "Ws2_32.lib")

#define DEFAULT_PORT "5000"
#define DEFAULT_PORT_2 "4000"
#define DEFAULT_BUFFER_LENGTH 512



class TCPServer
{
public:
	//ctor
	TCPServer();
	//dtor
	~TCPServer();
private:
	//data members
	std::string mServerIP;
	WSADATA wsaData;
	int iResult;
	SOCKET ListenSocket;
	std::vector<Room> rooms;
	std::vector<Client> clients;
	std::vector<std::thread*> threads;
	struct addrinfo* result = 0;
	struct addrinfo hints;
	struct addrinfo hintsServer;
	bool connected;
	Buffer* messageBuffer;
	bool dbConnected;
	SOCKET mConnectSocket = INVALID_SOCKET;
	struct addrinfo* ptr = NULL;
public:
	bool active = true;
	//methods
	void ConnectToServer();
	void AcceptClient();
	void ListenToClient(SOCKET clientSocket);
	void ListenToAuthServer(SOCKET clientSocket);
	void SendMessageToAllClients(std::string message, int roomIndex, SOCKET senderSocket);
	void SendMessageToClient(std::string message, SOCKET clientSocket);
	bool RoomNameExists(std::string roomName);
	int FindRoomByName(std::string roomName);
	Client * FindClientByPort(int portNumber);
	void BroadCastClientLeaving(Client* client, SOCKET clientSocket);
	void DumpToAllClients();
	void SingleRoomDump(std::string name);
	void SingleRoomDump(int index);
	bool ClientExists(std::string username);
};


enum MessageType
{
	MESSAGE = 0,
	JOINROOM,
	LEAVEROOM,
	REGISTER,
	REGISTERSUCCESS,
	REGISTERFAILUREACCOUNTEXISTS,
	REGISTERFAILURENOCONNECTIONTOSERVER,
	AUTHENTICATE,
	AUTHENTICATESUCCESS,
	AUTHENTICATEFAILUREWRONGPASSWORD,
	AUTHENTICATEFAILURENOCONNECTIONSERVER,
	AUTHENTICATEFAILURENOACCOUNT,
	MAKEROOM,
	GAMEROOMDUMP,
	SINGLEROOM
};

#endif // ! _TCPSERVER_HG_

