#include "TCPServer.h"
#include "Buffer.h"
#include <sstream>


static bool deleteAll(SOCKET * theElement) { delete theElement; return true; }

//constructor
TCPServer::TCPServer()
{
	messageBuffer = new Buffer(DEFAULT_BUFFER_LENGTH);
	std::cout << "Server running" << std::endl;
	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;
	connected = true;


	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (connected && iResult != 0) {
		printf("WSAStartup failed: %d\n", iResult);
		connected = false;
	}

	// Socket()
	ListenSocket = socket(AF_INET, SOCK_STREAM, 0);
	if (connected && ListenSocket == INVALID_SOCKET) {
		printf("socket() failed with error %d\n", WSAGetLastError());
		WSACleanup();
		connected = false;
	}

	// Bind()
	iResult = getaddrinfo(NULL, DEFAULT_PORT, &hints, &result);
	iResult = bind(ListenSocket, result->ai_addr, (int)result->ai_addrlen);
	if (connected && iResult == SOCKET_ERROR) {
		printf("bind() failed with error: %d\n", WSAGetLastError());
		freeaddrinfo(result);
		closesocket(ListenSocket);
		WSACleanup();
		connected = false;
	}

	// Listen()
	if (connected && listen(ListenSocket, 5)) {
		printf("listen() failed with error: %d\n", WSAGetLastError());
		closesocket(ListenSocket);
		WSACleanup();
		connected = false;
	}

	ZeroMemory(&hintsServer, sizeof(hintsServer));
	hintsServer.ai_family = AF_UNSPEC;
	hintsServer.ai_socktype = SOCK_STREAM;
	hintsServer.ai_protocol = IPPROTO_TCP;

	//assumed ip of home for this project
	mServerIP = "127.0.0.1";

	iResult = getaddrinfo(mServerIP.c_str(), DEFAULT_PORT_2, &hintsServer, &result);
	if (iResult != 0) {
		printf("getaddrinfo failed with error: %d\n", iResult);
		WSACleanup();
		connected = false;
	}

	ConnectToServer();

}

//destructor
TCPServer::~TCPServer()
{
	for (unsigned threadIndex = 0; threadIndex < threads.size(); ++threadIndex)
	{
		threads[threadIndex]->join();
	}
	delete messageBuffer;
	for (unsigned roomIndex = 0; roomIndex < rooms.size(); roomIndex++)
	{
		for (unsigned clientIndex = 0; clientIndex < rooms[roomIndex].clients.size(); ++clientIndex)
		{
			closesocket(rooms[roomIndex].clients[clientIndex].socket);
		}
	}
	closesocket(ListenSocket);
	WSACleanup();
}

//Name: ConnectToServer
//Purpose: to connect this server to the authentication server
//Accepts: nothing
//Returns: nothing
void TCPServer::ConnectToServer()
{
	for (ptr = result; ptr != NULL; ptr = ptr->ai_next) {
		mConnectSocket = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);
		if (mConnectSocket == INVALID_SOCKET) {
			printf("socket() failed with error: %d\n", iResult);
			WSACleanup();
			connected = false;
		}

		if (connected) {
			iResult = connect(mConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
			if (iResult == SOCKET_ERROR) {
				closesocket(mConnectSocket);
				mConnectSocket = INVALID_SOCKET;
				continue;
			}
		}

		break;
	}

	freeaddrinfo(result);

	if (mConnectSocket == INVALID_SOCKET) {
		printf("Unable to connect to server");
	}
	if (connected)
	{
		std::cout << "Connected to server" << std::endl;
	}
}

//Name: BroadCastClientLeaving
//Purpose: to let other clients know when a client leaves
//Accepts: a client object being the leaving client, and a socket object being the socket of the leaving client
//Returns: nothing
void TCPServer::BroadCastClientLeaving(Client* client, SOCKET clientSocket)
{
	//Broadcast to everyone that this client left the room
	for (int roomIndex = 0; roomIndex < rooms.size(); roomIndex++)
	{
		if (rooms[roomIndex].ClientExistsInRoom((*client)))
		{
			std::string userName = client->userName;
			int eraseIndex = rooms[roomIndex].FindClientInRoom((*client));
			rooms[roomIndex].clients.erase(rooms[roomIndex].clients.begin() + eraseIndex);
			SendMessageToAllClients(userName + " left the room ", roomIndex, clientSocket);
		}
	}
}

void TCPServer::DumpToAllClients()
{
	if (rooms.size() > 0)
	{
		//packet length will act as the number of rooms
		//then serialize that it will be a game dump
		//send that
		messageBuffer->SetWriteIndex(0);
		messageBuffer->WriteInt32BE(rooms.size());
		messageBuffer->WriteInt32BE(MessageType::GAMEROOMDUMP);
		int offset = 0;


		for (int i = 0; i < clients.size(); ++i)
		{
			if (clients[i].inMainList) {
				//send the number of rooms to the client so they know what to expect
				iResult = send((clients[i].socket), messageBuffer->GetBufferData(), 8, 0);
			}
		}
		for (int j = 0; j < rooms.size(); ++j)
		{
			messageBuffer->SetWriteIndex(0);
			//construct the string to be sent
			std::stringstream ss;
			std::string sIndex = std::to_string(j);
			sIndex.resize(10);
			std::string mapName = rooms[j].mapName;
			mapName.resize(10);
			std::string roomName = rooms[j].roomName;
			roomName.resize(10);
			std::string gameMode = rooms[j].gameMode;
			gameMode.resize(10);
			std::string players = std::to_string(rooms[j].clients.size()) + "/" + std::to_string(rooms[j].maxPlayers);
			players.resize(10);
			std::string hostName;
			hostName = rooms[j].clients[0].userName;
			hostName.resize(10);


			ss << sIndex << mapName << roomName << gameMode << players << hostName;

			std::string line = ss.str();
			messageBuffer->WriteInt32BE(line.size());
			messageBuffer->WriteString(line);
			for (int i = 0; i < clients.size(); ++i)
			{
				if (clients[i].inMainList) {
					//send the length of the string
					iResult = send((clients[i].socket), messageBuffer->GetBufferData(), 4, 0);

					//send the string itself
					iResult = send((clients[i].socket), messageBuffer->GetBufferData() + 4, line.size(), 0);
				}

			}
		}
	}
	else
	{
		//packet length wont matter for this //but serialize one anyway
		//then serialize that it will be a game dump
		//send that
		messageBuffer->SetWriteIndex(0);
		messageBuffer->WriteInt32BE(rooms.size());
		messageBuffer->WriteInt32BE(MessageType::GAMEROOMDUMP);

		for (int i = 0; i < clients.size(); ++i)
		{
			//send the number 0 so the client knows there are no games
			if (clients[i].inMainList) 
			{		
				iResult = send((clients[i].socket), messageBuffer->GetBufferData(), 8, 0);
			}
			
		}
	}
}

void TCPServer::SingleRoomDump(std::string name)
{
	int index = FindRoomByName(name);
	SingleRoomDump(index);
}

void TCPServer::SingleRoomDump(int index)
{
	if (rooms.size() > 0)
	{
		//packet length will act as the number of rooms
		//then serialize that it will be a game dump
		//send that
		messageBuffer->SetWriteIndex(0);
		messageBuffer->WriteInt32BE(rooms.size());
		messageBuffer->WriteInt32BE(MessageType::SINGLEROOM);
		int offset = 8;


		//construct the string to be sent
		std::stringstream ss;
		std::string sIndex = std::to_string(index);
		sIndex.resize(10);
		std::string mapName = rooms[index].mapName;
		mapName.resize(10);
		std::string roomName = rooms[index].roomName;
		roomName.resize(10);
		std::string gameMode = rooms[index].gameMode;
		gameMode.resize(10);
		std::string players = std::to_string(rooms[index].clients.size()) + "/" + std::to_string(rooms[index].maxPlayers);
		players.resize(10);
		std::string hostName;
		hostName = rooms[index].clients[0].userName;
		hostName.resize(10);


		ss << sIndex << mapName << roomName << gameMode << players << hostName;
		std::string line = ss.str();
		int lineSize = line.size();
		messageBuffer->WriteInt32BE(lineSize);
		offset += 4;
		messageBuffer->WriteString(line);
		int size = rooms[index].clients.size();
		for (int i = 0; i < size; ++i)
		{
			//send the header
			iResult = send((rooms[index].clients[i].socket), messageBuffer->GetBufferData(), 8, 0);
			//send the length of the string
			iResult = send((rooms[index].clients[i].socket), messageBuffer->GetBufferData() + 8, 4, 0);

			//send the string itself
			iResult = send((rooms[index].clients[i].socket), messageBuffer->GetBufferData() + 12, line.size(), 0);
		}

	}
}

bool TCPServer::ClientExists(std::string username)
{
	for (int i = 0; i < clients.size(); ++i)
	{
		if (clients[i].userName == username)
			return true;
	}
	return false;
}

//Name: AcceptClient
//Purpose: to accept a client and start up a thread to listen to it
//Accepts: nothing
//Returns: nothing
void TCPServer::AcceptClient()
{
	threads.push_back(new std::thread(&TCPServer::ListenToAuthServer, this, mConnectSocket));
	while (active)
	{
		SOCKET ClientSocket;
		// Accept()
		ClientSocket = accept(ListenSocket, NULL, NULL);
		if (ClientSocket == INVALID_SOCKET)
		{
			printf("accept() failed with error: %d\n", WSAGetLastError());
			closesocket(ListenSocket);
			//WSACleanup();
		}
		else
		{
			std::cout << "Client Connected" << std::endl;
			threads.push_back(new std::thread(&TCPServer::ListenToClient, this, ClientSocket));
		}
	}
}

//Name: ListenToAuthServer
//Purpose: to listen for responses from authentication server
//Accepts: a socket
//Returns: nothing
void TCPServer::ListenToAuthServer(SOCKET authServer)
{
	Client tempClient;
	tempClient.socket = authServer;

	do {
		//take in the header //getting packet length and message id
		iResult = recv(tempClient.socket, messageBuffer->GetBufferData(), 8, 0);

		if (iResult > 0)
		{
			messageBuffer->SetReadIndex(0);

			int packetLength = messageBuffer->ReadInt32BE();
			int message_id = messageBuffer->ReadInt32BE();

			if (packetLength > messageBuffer->GetBufferSize()) {
				messageBuffer->ResizeBuffer(packetLength);
			}

			messageBuffer->SetReadIndex(0);
			switch (message_id)
			{
			case MessageType::REGISTERSUCCESS:
			{
				//might have to do another recv here using the packetLength
				iResult = recv(tempClient.socket, messageBuffer->GetBufferData(), packetLength, 0);
				if (iResult > 0)
				{

					int requestID = messageBuffer->ReadInt32BE();
					//int userID = messageBuffer->ReadInt32BE();


					int totalLength = 8;
					messageBuffer->SetWriteIndex(0);
					messageBuffer->WriteInt32BE(totalLength);
					messageBuffer->WriteInt32BE(MessageType::REGISTERSUCCESS);


					iResult = send((requestID), messageBuffer->GetBufferData(), 8, 0);
				}
			}
			break;
			case MessageType::REGISTERFAILUREACCOUNTEXISTS:
			{
				//might have to do another recv here using the packetLength
				iResult = recv(tempClient.socket, messageBuffer->GetBufferData(), packetLength, 0);
				if (iResult > 0)
				{

					int requestID = messageBuffer->ReadInt32BE();

					int totalLength = 8;
					messageBuffer->SetWriteIndex(0);
					messageBuffer->WriteInt32BE(totalLength);
					messageBuffer->WriteInt32BE(MessageType::REGISTERFAILUREACCOUNTEXISTS);

					iResult = send((requestID), messageBuffer->GetBufferData(), 8, 0);
				}
			}
			break;
			case MessageType::AUTHENTICATESUCCESS:
			{
				//might have to do another recv here using the packetLength
				iResult = recv(tempClient.socket, messageBuffer->GetBufferData(), packetLength, 0);
				if (iResult > 0)
				{
					int requestID = messageBuffer->ReadInt32BE();

					//find the client this belongs to and send that to them
					Client* client = FindClientByPort(requestID);
					client->authenticated = true;

					//now send them any game information if it exists
					DumpToAllClients();
				}
			}
			break;
			case MessageType::AUTHENTICATEFAILURENOACCOUNT:
			{
				//might have to do another recv here using the packetLength
				iResult = recv(tempClient.socket, messageBuffer->GetBufferData(), packetLength, 0);
				if (iResult > 0)
				{
					int totalLength = 8;
					int requestID = messageBuffer->ReadInt32BE();
					messageBuffer->SetWriteIndex(0);
					messageBuffer->WriteInt32BE(totalLength);
					messageBuffer->WriteInt32BE(MessageType::AUTHENTICATEFAILURENOACCOUNT);



					Client* client = FindClientByPort(requestID);

					iResult = send(((*client).socket), messageBuffer->GetBufferData(), 8, 0);
				}
			}
			break;
			case MessageType::AUTHENTICATEFAILUREWRONGPASSWORD:
			{
				//might have to do another recv here using the packetLength
				iResult = recv(tempClient.socket, messageBuffer->GetBufferData(), packetLength, 0);
				if (iResult > 0)
				{
					int totalLength = 8;
					int requestID = messageBuffer->ReadInt32BE();
					messageBuffer->SetWriteIndex(0);
					messageBuffer->WriteInt32BE(totalLength);
					messageBuffer->WriteInt32BE(MessageType::AUTHENTICATEFAILUREWRONGPASSWORD);



					Client* client = FindClientByPort(requestID);


					iResult = send(((*client).socket), messageBuffer->GetBufferData(), 8, 0);
				}
			}
			break;
			}
		}


		if (iResult == SOCKET_ERROR) {
			printf("recv failed with error: %d\n", WSAGetLastError());
			closesocket(tempClient.socket);
		}

		if (iResult > 0) {
			printf("Bytes Received: %d\n", iResult);
		}

		if (iResult == 0) {
			printf("Connection closing...\n");
		}

		std::cout << "Data recieved" << std::endl;

	} while (iResult > 0 && active);
}

//Name: ListenToClient
//Purpose: to listen for messages from a specific 
//Accepts: a socket
//Returns: nothing
void TCPServer::ListenToClient(SOCKET clientSocket)
{
	Client tempClient;
	tempClient.socket = clientSocket;

	do {
		//take in the header //getting packet length and message id
		iResult = recv(tempClient.socket, messageBuffer->GetBufferData(), sizeof(uint32_t) * 2, 0);

		if (iResult > 0)
		{
			messageBuffer->SetReadIndex(0);

			int packetLength = messageBuffer->ReadInt32BE();
			int message_id = messageBuffer->ReadInt32BE();

			if (packetLength > messageBuffer->GetBufferSize()) {
				messageBuffer->ResizeBuffer(packetLength);
			}

			switch (message_id)
			{
				//Leaving a room
			case MessageType::LEAVEROOM:
			{
				//have to do another recv here using the packetLength
				iResult = recv(tempClient.socket, messageBuffer->GetBufferData(), packetLength, 0);
				if (iResult > 0)
				{
					messageBuffer->SetReadIndex(0);
					//get the room name
					std::string roomName;
					std::string username;
					int roomToJoinLength = messageBuffer->ReadInt32BE();
					roomName = messageBuffer->ReadString(roomToJoinLength);


					if (RoomNameExists(roomName))
					{
						//remove the client from the room //if they are in the existing room
						int tempRoomIndex = FindRoomByName(roomName);

						if (rooms[tempRoomIndex].ClientExistsInRoom(tempClient))
						{

							//If the client leaving the room was the host, then delete the room
							//and update all the clients back into the main list
							if (tempClient.socket == rooms[tempRoomIndex].clients[0].socket)
							{
								for (int clientIndex = 0; clientIndex < rooms[tempRoomIndex].clients.size(); ++clientIndex)
								{
									FindClientByPort(rooms[tempRoomIndex].clients[clientIndex].socket)->inMainList = true;
									SendMessageToClient("Host left lobby", rooms[tempRoomIndex].clients[clientIndex].socket);
								}

								rooms.erase(rooms.begin() + tempRoomIndex);
							}
							//if it wasnt the host then do a specific room dump
							else
							{
								FindClientByPort(tempClient.socket)->inMainList = true;
								std::string name = FindClientByPort(tempClient.socket)->userName;

								for (int clientIndex = 0; clientIndex < rooms[tempRoomIndex].clients.size(); ++clientIndex)
								{				
									SendMessageToClient(name+" left lobby", rooms[tempRoomIndex].clients[clientIndex].socket);
								}
								int eraseIndex = rooms[tempRoomIndex].FindClientInRoom(tempClient);
								rooms[tempRoomIndex].clients.erase(rooms[tempRoomIndex].clients.begin() + eraseIndex);
								SingleRoomDump(tempRoomIndex);
							}
							

							

							//SendMessageToAllClients(username + " left the room ", FindRoomByName(roomName), clientSocket);
						}

						DumpToAllClients();
					}
				}
				else
				{
					//Broadcast to everyone that this client left the room
					for (int roomIndex = 0; roomIndex < rooms.size(); roomIndex++)
					{
						if (rooms[roomIndex].ClientExistsInRoom(tempClient))
						{
							std::string userName = tempClient.userName;
							int eraseIndex = rooms[roomIndex].FindClientInRoom(tempClient);
							rooms[roomIndex].clients.erase(rooms[roomIndex].clients.begin() + eraseIndex);
							SendMessageToAllClients(userName + " left the room ", roomIndex, clientSocket);
						}
					}
				}

			}
			break;
			case MessageType::MAKEROOM:
			{
				iResult = recv(tempClient.socket, messageBuffer->GetBufferData(), packetLength, 0);
				if (iResult > 0)
				{
					messageBuffer->SetReadIndex(0);
					std::string mapName;

					int mapNameLength = messageBuffer->ReadInt32BE();
					mapName = messageBuffer->ReadString(mapNameLength);

					std::string roomName;

					int roomNameLength = messageBuffer->ReadInt32BE();
					roomName = messageBuffer->ReadString(roomNameLength);

					std::string gameMode;

					int gameModeLength = messageBuffer->ReadInt32BE();
					gameMode = messageBuffer->ReadString(gameModeLength);

					int players = messageBuffer->ReadInt32BE();


					//Create a room with these specifications
					//add this client to that room
					//turn off the client from the main list
					//create the new room using the room name
					if (!RoomNameExists(roomName)) {
						Room newRoom;
						newRoom.clients.push_back(*FindClientByPort(tempClient.socket));
						newRoom.roomName = roomName;
						newRoom.mapName = mapName;
						newRoom.gameMode = gameMode;
						newRoom.maxPlayers = players;
						rooms.push_back(newRoom);

						FindClientByPort(tempClient.socket)->inMainList = false;
						SingleRoomDump(rooms.size() - 1);
					}
					else
					{
						SendMessageToClient("Lobby name already taken", tempClient.socket);
					}
					DumpToAllClients();
				}
			}
			break;
			//Joining a room
			case MessageType::JOINROOM:
			{
				//have to do another recv here using the packetLength
				iResult = recv(tempClient.socket, messageBuffer->GetBufferData(), packetLength, 0);
				if (iResult > 0)
				{
					messageBuffer->SetReadIndex(0);
					//get the room name
					std::string roomName;

					int roomToJoinLength = messageBuffer->ReadInt32BE();
					roomName = messageBuffer->ReadString(roomToJoinLength);



					if (RoomNameExists(roomName))
					{
						//add the client to the existing room //if the client is not already in the room
						Room tempRoom = rooms[FindRoomByName(roomName)];
						if (!tempRoom.ClientExistsInRoom(tempClient) && tempRoom.clients.size() < tempRoom.maxPlayers)
						{
							FindClientByPort(tempClient.socket)->inMainList = false;
							int tempRoomIndex = FindRoomByName(roomName);
							rooms[tempRoomIndex].clients.push_back(*FindClientByPort(tempClient.socket));
							std::string name = FindClientByPort(tempClient.socket)->userName;

							for (int clientIndex = 0; clientIndex < rooms[tempRoomIndex].clients.size(); ++clientIndex)
							{
								SendMessageToClient(name + " joined lobby", rooms[tempRoomIndex].clients[clientIndex].socket);
							}
							SingleRoomDump(roomName);
						}
						else if (tempRoom.clients.size() == tempRoom.maxPlayers)
						{
							SendMessageToClient("This Lobby is full", tempClient.socket);
						}
						
					}
				}

			}
			break;
			case MessageType::MESSAGE:
			{
				//No message functionality in third project
			}
			break;
			case MessageType::REGISTER:
			{
				//might have to do another recv here using the packetLength
				iResult = recv(tempClient.socket, messageBuffer->GetBufferData(), packetLength, 0);
				if (iResult > 0)
				{
					messageBuffer->SetReadIndex(0);
					//get the room name
					std::string email;
					std::string password;
					int emailLength = messageBuffer->ReadInt32BE();
					email = messageBuffer->ReadString(emailLength);

					//get the username
					int passwordLength = messageBuffer->ReadInt32BE();
					password = messageBuffer->ReadString(passwordLength);

					try {
						messageBuffer->SetWriteIndex(0);
						//serialize the total length
						int totalLength = 12 + email.size() + password.size();
						messageBuffer->WriteInt32BE(totalLength);
						//serialize the message id
						messageBuffer->WriteInt32BE(MessageType::REGISTER);

						//serialize room name length
						messageBuffer->WriteInt32BE(email.size());
						//serialize room name
						messageBuffer->WriteString(email);
						//serialize the username length
						messageBuffer->WriteInt32BE(password.size());
						//serialize the username
						messageBuffer->WriteString(password);
						messageBuffer->WriteInt32BE(tempClient.socket);

						//actually sending the data
						iResult = send(mConnectSocket, messageBuffer->GetBufferData(), sizeof(uint32_t) * 2, 0);
						iResult = send(mConnectSocket, messageBuffer->GetBufferData() + 8, totalLength, 0);
						if (iResult == SOCKET_ERROR) {
							printf("socket() failed with error: %d\n", iResult);
							closesocket(mConnectSocket);
							WSACleanup();
							connected = false;
						}
					}
					catch (...)
					{
						messageBuffer->SetWriteIndex(0);
						//serialize the total length
						int totalLength = 0;
						messageBuffer->WriteInt32BE(totalLength);
						//serialize the message id
						messageBuffer->WriteInt32BE(MessageType::REGISTERFAILURENOCONNECTIONTOSERVER);


						//actually sending the data
						iResult = send(mConnectSocket, messageBuffer->GetBufferData(), sizeof(uint32_t) * 2, 0);
						if (iResult == SOCKET_ERROR) {
							printf("socket() failed with error: %d\n", iResult);
							closesocket(mConnectSocket);
							WSACleanup();
							connected = false;
						}
					}


				}
				else
				{
					BroadCastClientLeaving(&tempClient, clientSocket);
				}
			}
			break;
			case MessageType::AUTHENTICATE:
			{
				//might have to do another recv here using the packetLength
				iResult = recv(tempClient.socket, messageBuffer->GetBufferData(), packetLength, 0);
				if (iResult > 0)
				{
					messageBuffer->SetReadIndex(0);
					//get the room name
					std::string email;
					std::string password;
					int emailLength = messageBuffer->ReadInt32BE();
					email = messageBuffer->ReadString(emailLength);


					//get the username
					int passwordLength = messageBuffer->ReadInt32BE();
					password = messageBuffer->ReadString(passwordLength);

					//make a client here and add it to the main list of clients
					if (!ClientExists(email))
					{
						Client* client = new Client();
						client->socket = tempClient.socket;
						client->authenticated = false;
						client->userName = email;
						tempClient.userName = email;
						clients.push_back(*client);

						try {
							messageBuffer->SetWriteIndex(0);
							//serialize the total length
							int totalLength = 12 + email.size() + password.size();
							messageBuffer->WriteInt32BE(totalLength);
							//serialize the message id
							messageBuffer->WriteInt32BE(MessageType::AUTHENTICATE);

							//serialize room name length
							messageBuffer->WriteInt32BE(email.size());
							//serialize room name
							messageBuffer->WriteString(email);
							//serialize the username length
							messageBuffer->WriteInt32BE(password.size());
							//serialize the username
							messageBuffer->WriteString(password);
							messageBuffer->WriteInt32BE(tempClient.socket);

							//actually sending the data
							iResult = send(mConnectSocket, messageBuffer->GetBufferData(), sizeof(uint32_t) * 2, 0);
							iResult = send(mConnectSocket, messageBuffer->GetBufferData() + 8, totalLength, 0);
							if (iResult == SOCKET_ERROR) {
								printf("socket() failed with error: %d\n", iResult);
								closesocket(mConnectSocket);
								WSACleanup();
								connected = false;
							}
						}
						catch (...)
						{
							messageBuffer->SetWriteIndex(0);
							//serialize the total length
							int totalLength = 0;
							messageBuffer->WriteInt32BE(totalLength);
							//serialize the message id
							messageBuffer->WriteInt32BE(MessageType::AUTHENTICATEFAILURENOCONNECTIONSERVER);


							//actually sending the data
							iResult = send(mConnectSocket, messageBuffer->GetBufferData(), sizeof(uint32_t) * 2, 0);
							if (iResult == SOCKET_ERROR) {
								printf("socket() failed with error: %d\n", iResult);
								closesocket(mConnectSocket);
								WSACleanup();
								connected = false;
							}
						}

					}
					else
					{
						//tell the client that name is already taken
						SendMessageToClient("That user is already logged in", tempClient.socket);
					}


				}
				else
				{
					BroadCastClientLeaving(&tempClient, clientSocket);
				}
			}
			break;

			}
		}
		else
		{
			BroadCastClientLeaving(&tempClient, clientSocket);
		}


		if (iResult == SOCKET_ERROR) {
			printf("recv failed with error: %d\n", WSAGetLastError());
			closesocket(tempClient.socket);
		}

		if (iResult > 0) {
			printf("Bytes Received: %d\n", iResult);
		}

		if (iResult == 0) {
			printf("Connection closing...\n");
		}

		std::cout << "Data recieved" << std::endl;

	} while (iResult > 0 && active);
}

//Name: SendMessageToAllClients
//Purpose: to send a message to all clients
//Accepts: a string representing the message to send, the index of the room, and the socket respresenting
//         the sender of the message
//Returns: nothing
void TCPServer::SendMessageToAllClients(std::string message, int roomIndex, SOCKET senderSocket)
{
}

void TCPServer::SendMessageToClient(std::string message, SOCKET clientSocket)
{
	int totalLength = 4 + message.size();
	messageBuffer->SetWriteIndex(0);
	messageBuffer->WriteInt32BE(totalLength);
	messageBuffer->WriteInt32BE(MessageType::MESSAGE);

	messageBuffer->WriteInt32BE(message.size());
	messageBuffer->WriteString(message);

	iResult = send(clientSocket, messageBuffer->GetBufferData(), sizeof(uint32_t) * 2, 0);
	iResult = send(clientSocket, messageBuffer->GetBufferData() + 8, totalLength, 0);
}

//Name: RoomNameExists
//Purpose: to check if a room exists
//Accepts: the room name as a string
//Returns: a bool saying if the room exists or not
bool TCPServer::RoomNameExists(std::string roomName)
{
	for (unsigned roomIndex = 0; roomIndex < rooms.size(); roomIndex++)
	{
		if (rooms[roomIndex].roomName == roomName)
			return true;
	}
	return false;
}

//Name: FindRoomByName
//Purpose: to find a room by name
//Accepts: the room name as a string
//Returns: an int representing the index of the room
int TCPServer::FindRoomByName(std::string roomName)
{
	for (unsigned roomIndex = 0; roomIndex < rooms.size(); roomIndex++)
	{
		if (rooms[roomIndex].roomName == roomName)
			return roomIndex;
	}
	return -1;
}

//Name: FindClientByPort
//Purpose: to find a client using a certain port on this server
//Accepts: an int being the port number
//Returns: a socket pointer representing the client to send a message to
Client * TCPServer::FindClientByPort(int portNumber)
{
	std::cout << "Found a client by port" << std::endl;
	for (unsigned clientIndex = 0; clientIndex < clients.size(); clientIndex++)
	{
		if (clients[clientIndex].socket == portNumber)
		{
			return &clients[clientIndex];
		}
	}

	return nullptr;
}
