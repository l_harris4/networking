#include "Room.h"

//constructor
Room::Room()
{
}

//destructor
Room::~Room()
{
}

//Name: ClientExistsInRoom
//Purpose: to see if a client is in a room
//Accepts: a client object
//Returns: a bool saying if the client is in the room or not
bool Room::ClientExistsInRoom(Client& client)
{
	for (unsigned clientIndex = 0; clientIndex < clients.size(); clientIndex++)
	{
		if (client == clients[clientIndex])
			return true;
	}
	return false;
}

//Name: FindClientInRoom
//Purpose: to find the exact index of a client in a room
//Accepts: a client object
//Returns: an int representing where the client is in the room
int Room::FindClientInRoom(Client& client)
{
	for (unsigned clientIndex = 0; clientIndex < clients.size(); clientIndex++)
	{
		if (client == clients[clientIndex])
			return clientIndex;
	}
	return -1;
}
