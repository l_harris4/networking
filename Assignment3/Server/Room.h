#ifndef _ROOM_HG_
#define _ROOM_HG_

#include <vector>
#include <string>
#include "Client.h"

class Room
{
public:
	//ctor
	Room();
	//dtor
	~Room();
	//data members
	std::vector<Client> clients;
	std::string roomName;
	std::string mapName;
	std::string gameMode;
	int maxPlayers;
	//methods
	bool ClientExistsInRoom(Client& client);
	int FindClientInRoom(Client& client);
};

#endif
