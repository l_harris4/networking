#include "DatabaseConnector.h"
#include <iostream>

using namespace std;

//ctor
DatabaseConnector::DatabaseConnector()
{

}

//dtor
DatabaseConnector::~DatabaseConnector()
{

}

//Name: ConnectToDatabase
//Purpose: creating a connection to a mysql database
//Accepts: 4 strings, being the name of the server, the username, the password, and the schema name
//Returns: a bool saying if it worked or not
bool DatabaseConnector::ConnectToDatabase(const std::string &server,
	const std::string &username,
	const std::string &password,
	const std::string &schema)
{
	try
	{
		driver = get_driver_instance();
		con = driver->connect(server, username, password);
		con->setSchema(schema);
	}
	catch (sql::SQLException &exception)
	{
		cout << "# ERR: SQLException in " << __FILE__;
		cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
		cout << "# ERR: " << exception.what();
		cout << " (MySQL error code: " << exception.getErrorCode();
		cout << ", SQLState: " << exception.getSQLState() << " )" << endl;
		return false;
	}

	return true;
}

//Name: Execute
//Purpose: to excute sql against the databse
//Accepts: a string being the sql to be run
//Returns: a bool stating if it worked or not
bool DatabaseConnector::Execute(const std::string &sql)
{
	try
	{
		pstmt = con->prepareStatement(sql);
		return pstmt->execute();
	}
	catch (sql::SQLException &exception)
	{
		cout << "# ERR: SQLException in " << __FILE__;
		cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
		cout << "# ERR: " << exception.what();
		cout << " (MySQL error code: " << exception.getErrorCode();
		cout << ", SQLState: " << exception.getSQLState() << " )" << endl;
		return false;
	}
}

//Name: ExecuteQuery
//Purpose: to excute a query against the database intenting to retrieve data
//Accepts: a string being the sql to be run
//Returns: a result set being the data that was retrieved
sql::ResultSet* DatabaseConnector::ExecuteQuery(const std::string &sql)
{
	try
	{
		pstmt = con->prepareStatement(sql);
		return pstmt->executeQuery();
	}
	catch (sql::SQLException &exception)
	{
		cout << "# ERR: SQLException in " << __FILE__;
		cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
		cout << "# ERR: " << exception.what();
		cout << " (MySQL error code: " << exception.getErrorCode();
		cout << ", SQLState: " << exception.getSQLState() << " )" << endl;
		return false;
	}
}

//Name: ExecuteUpdate
//Purpose: to excute a query against the database intenting to change data
//Accepts: a string being the sql to be run
//Returns: an int representing the number of rows changed
int DatabaseConnector::ExecuteUpdate(const std::string &sql)
{
	try
	{
		pstmt = con->prepareStatement(sql);
		return pstmt->executeUpdate();
	}
	catch (sql::SQLException &exception)
	{
		cout << "# ERR: SQLException in " << __FILE__;
		cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
		cout << "# ERR: " << exception.what();
		cout << " (MySQL error code: " << exception.getErrorCode();
		cout << ", SQLState: " << exception.getSQLState() << " )" << endl;
		return false;
	}
}
