#include "../Server/Buffer.h"
#include "../shared/chatapp.pb.h"
#include "TCPServerAuth.h"

using namespace std;

int main() {
	//create the server
	TCPServerAuth server;

	//allow the server to accept clients
	thread acceptClientsThread(&TCPServerAuth::AcceptClient, server);
	string test = "";

	while (test != "q")
	{
		cin >> test;
		if (test == "q")
		{
			acceptClientsThread.join();
			server.active = false;

		}
	}
	system("pause");
}