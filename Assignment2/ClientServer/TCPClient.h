#ifndef _TCPCLIENT_HG_
#define _TCPCLIENT_HG_
#define UNICODE
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <WinSock2.h>
#include <ws2tcpip.h>

#include <iostream>
#include <string>
#include <set>
#include <algorithm>

#include "../Server/Buffer.h"
#include "../shared/chatapp.pb.h"

#pragma comment (lib, "Ws2_32.lib")

#define DEFAULT_PORT "5000"
#define DEFAULT_BUFFER_LENGTH 512

class TCPClient
{
public:
	//ctor
	TCPClient();
	//dtor
	~TCPClient();
private:
	//data members
	std::string mServerIP;
	WSADATA mWsaData;
	SOCKET mConnectSocket = INVALID_SOCKET;

	struct addrinfo* result = NULL;
	struct addrinfo* ptr = NULL;
	struct addrinfo hints;

	int iResult;

	Buffer* messageBuffer;
	

public:
	bool connected;
	std::vector<std::string> roomNames;
	std::string username;
	unsigned int roomNameIndex;

	//methods
	void ConnectToServer();
	void JoinRoom(std::string roomName);
	void LeaveRoom(std::string roomName);
	void SendMessageToServer(std::string message, std::string roomName);
	void ListenToServer();
	void InsertRoomName(std::string roomName);
	void DeleteRoomName(std::string roomName);
	void Register(std::string email, std::string password);
	void Authenticate(std::string email, std::string password);
	
};

enum MessageType
{
	MESSAGE = 0,
	JOINROOM,
	LEAVEROOM,
	REGISTER,
	REGISTERSUCCESS,
	REGISTERFAILURE,
	AUTHENTICATE,
	AUTHENTICATESUCCESS,
	AUTHENTICATEFAILURE
};

#endif // !_TCPCLIENT_HG_

