#include "TCPClient.h"

//constructor
TCPClient::TCPClient()
{
	roomNameIndex = 0;
	messageBuffer = new Buffer(DEFAULT_BUFFER_LENGTH);

	std::cout << "Client running" << std::endl;
	connected = true;

	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &mWsaData);
	if (iResult != 0) {
		printf("WSAStartup failed: %d\n", iResult);
		connected = false;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	//assumed ip of home for this project
	mServerIP = "127.0.0.1";

	iResult = getaddrinfo(mServerIP.c_str(), DEFAULT_PORT, &hints, &result);
	if (iResult != 0) {
		printf("getaddrinfo failed with error: %d\n", iResult);
		WSACleanup();
		connected = false;
	}

}

//destructor
TCPClient::~TCPClient()
{
	delete messageBuffer;
	iResult = shutdown(mConnectSocket, SD_SEND);
	if (iResult == SOCKET_ERROR) {
		printf("shutdown() failed with error: %d\n", iResult);
		closesocket(mConnectSocket);
		WSACleanup();
		connected = false;
	}

	closesocket(mConnectSocket);
	WSACleanup();
}

//Name: ConnectToServer
//Purpose: to connect the client to the server
//Accepts: nothing
//Returns: nothing
void TCPClient::ConnectToServer()
{
	for (ptr = result; ptr != NULL; ptr = ptr->ai_next) {
		mConnectSocket = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);
		if (mConnectSocket == INVALID_SOCKET) {
			printf("socket() failed with error: %d\n", iResult);
			WSACleanup();
			connected = false;
		}

		if (connected) {
			iResult = connect(mConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
			if (iResult == SOCKET_ERROR) {
				closesocket(mConnectSocket);
				mConnectSocket = INVALID_SOCKET;
				continue;
			}
		}

		break;
	}

	freeaddrinfo(result);

	if (mConnectSocket == INVALID_SOCKET) {
		printf("Unable to connect to server");
		WSACleanup();
		connected = false;
	}
	if (connected)
	{
		std::cout << "Connected to server" << std::endl;
	}
}

//Name: JoinRoom
//Purpose: to join the client to a room on the server
//Accepts: a string representing the room name that the client wants to join
//Returns: nothing
void TCPClient::JoinRoom(std::string roomName)
{
	chatapp::Header header;
	int totalLength = 8 + roomName.size() + username.size();
	header.set_messagesize(totalLength);
	header.set_messagetype(MessageType::JOINROOM);
	std::string serializedString;
	header.SerializeToString(&serializedString);
	messageBuffer->SetWriteIndex(0);
	messageBuffer->WriteString(serializedString);

	chatapp::JoinRoom joinRoomCommand;
	joinRoomCommand.set_roomname(roomName);
	joinRoomCommand.set_username(username);
	std::string serializedString2;
	joinRoomCommand.SerializeToString(&serializedString2);
	messageBuffer->WriteString(serializedString2);

	//header size
	int headerSize = header.ByteSize();
	//actually sending the data
	iResult = send(mConnectSocket, messageBuffer->GetBufferData(), 4, 0);
	iResult = send(mConnectSocket, messageBuffer->GetBufferData() + 4, totalLength, 0);
	if (iResult == SOCKET_ERROR) {
		printf("socket() failed with error: %d\n", iResult);
		closesocket(mConnectSocket);
		WSACleanup();
		connected = false;
	}
	InsertRoomName(roomName);
}

//Name: LeaveRoom
//Purpose: to take this client out of a room on the server
//Accepts: a string representing the room name that the client wants to leave
//Returns: nothing
void TCPClient::LeaveRoom(std::string roomName)
{
	//connectToServer();
	//send username and room you want to join
	chatapp::Header header;
	int totalLength = 8 + roomName.size() + username.size();
	header.set_messagesize(totalLength);
	header.set_messagetype(MessageType::JOINROOM);
	std::string serializedString;
	header.SerializeToString(&serializedString);
	messageBuffer->SetWriteIndex(0);
	messageBuffer->WriteString(serializedString);

	chatapp::LeaveRoom leaveRoomCommand;
	leaveRoomCommand.set_roomname(roomName);
	leaveRoomCommand.set_username(username);
	std::string serializedString2;
	leaveRoomCommand.SerializeToString(&serializedString2);
	messageBuffer->WriteString(serializedString2);

	//actually sending the data
	iResult = send(mConnectSocket, messageBuffer->GetBufferData(), 4, 0);
	iResult = send(mConnectSocket, messageBuffer->GetBufferData() + 4, totalLength, 0);
	if (iResult == SOCKET_ERROR) {
		printf("socket() failed with error: %d\n", iResult);
		closesocket(mConnectSocket);
		WSACleanup();
		connected = false;
		std::cout << "Lost connection to server" << std::endl;
	}
	DeleteRoomName(roomName);
	std::cout << "You have just left the room: " << roomName << std::endl;
}

//Name: SendMessageToServer
//Purpose: to send a string message to the server
//Accepts: a string representing the room name that the client wants to leave,
//         and the message itself
//Returns: nothing
void TCPClient::SendMessageToServer(std::string message, std::string roomName)
{
	if (connected)
	{
		try
		{
			//prefix the message with out username before sending it out
			message = username + ":" + message;
			messageBuffer->SetWriteIndex(0);

			chatapp::Header header;
			int totalLength = 8 + roomName.size() + message.size();
			header.set_messagesize(totalLength);
			header.set_messagetype(MessageType::MESSAGE);
			std::string serializedString;
			header.SerializeToString(&serializedString);
			messageBuffer->SetWriteIndex(0);
			messageBuffer->WriteString(serializedString);

			chatapp::Msg messageCommand;
			messageCommand.set_roomname(roomName);
			messageCommand.set_message(message);
			std::string serializedString2;
			messageCommand.SerializeToString(&serializedString2);
			messageBuffer->WriteString(serializedString2);

			//actually sending the data
			iResult = send(mConnectSocket, messageBuffer->GetBufferData(), 4, 0);
			iResult = send(mConnectSocket, messageBuffer->GetBufferData() + 4, totalLength, 0);
			if (iResult == SOCKET_ERROR) {
				closesocket(mConnectSocket);
				WSACleanup();
				connected = false;
			}
			if (iResult == 0) {
				closesocket(mConnectSocket);
				WSACleanup();
				connected = false;
			}
		}
		catch (...)
		{
			//Catch any exceptions that might come up to multithreading woes
		}
	}
}

//Name: ListenToServer
//Purpose: to receive messages from the server
//Accepts: nothing
//Returns: nothing
void TCPClient::ListenToServer()
{
	do {
		//take in the header //getting packet length and message id
		iResult = recv(mConnectSocket, messageBuffer->GetBufferData(), 4, 0);

		if (iResult == SOCKET_ERROR) {
			printf("recv failed with error: %d\n", WSAGetLastError());
			closesocket(mConnectSocket);
			WSACleanup();
			connected = false;
			std::cout << "Lost connection to server" << std::endl;
		}

		if (iResult == 0) {
			closesocket(mConnectSocket);
			WSACleanup();
			connected = false;
			std::cout << "Lost connection to server" << std::endl;
		}

		if (connected) 
		{
			messageBuffer->SetReadIndex(0);
			chatapp::Header deserialized_header;
			deserialized_header.ParseFromString(messageBuffer->ReadString(4));

			int packetLength = deserialized_header.messagesize();
			int message_id = deserialized_header.messagetype();

			//if incoming packet is larger than buffer, increase buffer size
			if (packetLength > messageBuffer->GetBufferSize()) {
				messageBuffer->ResizeBuffer(packetLength);
			}

			switch (message_id)
			{
			case MessageType::MESSAGE:
			{
				//might have to do another recv here using the packetLength
				iResult = recv(mConnectSocket, messageBuffer->GetBufferData(), packetLength, 0);
				if (iResult == SOCKET_ERROR) {
					printf("recv failed with error: %d\n", WSAGetLastError());
					closesocket(mConnectSocket);
					WSACleanup();
					connected = false;
					std::cout << "Lost connection to server" << std::endl;
				}

				if (iResult == 0) {
					closesocket(mConnectSocket);
					WSACleanup();
					connected = false;
					std::cout << "Lost connection to server" << std::endl;
				}
				if (connected)
				{
					messageBuffer->SetReadIndex(0);
					chatapp::Msg deserializedMessage;
					//maybe the line below should use packetLength
					deserializedMessage.ParseFromString(messageBuffer->ReadString(packetLength));
					std::string roomName = deserializedMessage.roomname();
					std::string message = deserializedMessage.message();

					std::cout << message << std::endl;
				}
			}
				break;
			case MessageType::REGISTERSUCCESS:
			{
				//might have to do another recv here using the packetLength
				iResult = recv(mConnectSocket, messageBuffer->GetBufferData(), packetLength, 0);
				if (iResult > 0)
				{
					messageBuffer->SetReadIndex(0);
					chatapp::CreateAccountWebSuccess deserializedMessage;
					//maybe the line below should use packetLength
					deserializedMessage.ParseFromString(messageBuffer->ReadString(packetLength));
					int requestID = deserializedMessage.requestid();
					int userID = deserializedMessage.userid();

					std::cout << "Registration successful" << std::endl;
				}
				
			}
			break;
			case MessageType::REGISTERFAILURE:
			{
				//might have to do another recv here using the packetLength
				iResult = recv(mConnectSocket, messageBuffer->GetBufferData(), packetLength, 0);
				if (iResult > 0)
				{
					messageBuffer->SetReadIndex(0);
					chatapp::CreateAccountWebFailure deserializedMessage;
					//maybe the line below should use packetLength
					deserializedMessage.ParseFromString(messageBuffer->ReadString(packetLength));
					chatapp::CreateAccountWebFailure_ReasonType reason = deserializedMessage.reason();

					switch (reason)
					{
					case chatapp::CreateAccountWebFailure_ReasonType::CreateAccountWebFailure_ReasonType_ACCOUNT_ALREADY_EXISTS:
						std::cout << "Account with that email already exists" << std::endl;
						break;
					case chatapp::CreateAccountWebFailure_ReasonType::CreateAccountWebFailure_ReasonType_INTERNAL_SERVER_ERROR:
						std::cout << "Internal server error" << std::endl;
						break;
					}
				}
			}
			break;
			case MessageType::AUTHENTICATESUCCESS:
			{
				//might have to do another recv here using the packetLength
				iResult = recv(mConnectSocket, messageBuffer->GetBufferData(), packetLength, 0);
				if (iResult > 0)
				{
					messageBuffer->SetReadIndex(0);
					chatapp::AuthenticateWebSuccess deserializedMessage;
					//maybe the line below should use packetLength
					deserializedMessage.ParseFromString(messageBuffer->ReadString(packetLength));
					std::string date = deserializedMessage.creationdate();
					std::cout << "Account is valid" << std::endl;
					std::cout << "Created: " << date << std::endl;
				}
			}
			break;
			case MessageType::AUTHENTICATEFAILURE:
			{
				//might have to do another recv here using the packetLength
				iResult = recv(mConnectSocket, messageBuffer->GetBufferData(), packetLength, 0);
				if (iResult > 0)
				{
					messageBuffer->SetReadIndex(0);
					chatapp::AuthenticateWebFailure deserializedMessage;
					//maybe the line below should use packetLength
					deserializedMessage.ParseFromString(messageBuffer->ReadString(packetLength));
					chatapp::AuthenticateWebFailure_FailureType reason = deserializedMessage.reason();

					switch (reason)
					{
					case chatapp::AuthenticateWebFailure_FailureType::AuthenticateWebFailure_FailureType_ACCOUNT_DOES_NOT_EXIST:
						std::cout << "Account does not exist" << std::endl;
						break;
					case chatapp::AuthenticateWebFailure_FailureType::AuthenticateWebFailure_FailureType_INTERNAL_SERVER_ERROR:
						std::cout << "Internal server error" << std::endl;
						break;
					case chatapp::AuthenticateWebFailure_FailureType::AuthenticateWebFailure_FailureType_INVALID_PASSWORD:
						std::cout << "Invalid password" << std::endl;
						break;
					}
				}
			}
			break;
			}
		}

	} while (iResult > 0 && connected);
}

//Name: InsertRoomName
//Purpose: to add a room name to the local copy of what rooms the client is in
//Accepts: the name of the room to join
//Returns: nothing
void TCPClient::InsertRoomName(std::string roomName)
{
	roomNames.push_back(roomName);
}

//Name: DeleteRoomName
//Purpose: to remove a room name from the local copy of what rooms the client is in
//Accepts: the name of the room to leave
//Returns: nothing
void TCPClient::DeleteRoomName(std::string roomName)
{
	roomNames.erase(std::remove(roomNames.begin(), roomNames.end(), roomName), roomNames.end());
	if (roomNameIndex >= roomNames.size())
	{
		roomNameIndex = 0;
	}
}

//Name: Register
//Purpose: to register a user in the database
//Accepts: a string being the email and a string being the password of a user
//Returns: nothing
void TCPClient::Register(std::string email, std::string password)
{
	chatapp::Header header;
	int totalLength = 12 + email.size() + password.size();
	header.set_messagesize(totalLength);
	header.set_messagetype(MessageType::REGISTER);
	std::string serializedString;
	header.SerializeToString(&serializedString);
	messageBuffer->SetWriteIndex(0);
	messageBuffer->WriteString(serializedString);
	


	chatapp::CreateAccountWeb registerCommand;
	registerCommand.set_email(email);
	registerCommand.set_password(password);
	registerCommand.set_requestid(-1);
	std::string serializedString2;
	registerCommand.SerializeToString(&serializedString2);
	messageBuffer->WriteString(serializedString2);

	//header size
	int headerSize = header.ByteSize();
	//actually sending the data
	iResult = send(mConnectSocket, messageBuffer->GetBufferData(), 4, 0);
	iResult = send(mConnectSocket, messageBuffer->GetBufferData() + 4, totalLength, 0);
	if (iResult == SOCKET_ERROR) {
		printf("socket() failed with error: %d\n", iResult);
		closesocket(mConnectSocket);
		WSACleanup();
		connected = false;
	}
}


//Name: Authenticate
//Purpose: to test if a user is in the database with a specific password
//Accepts: a string being the email and a string being the password of a user
//Returns: nothing
void TCPClient::Authenticate(std::string email, std::string password)
{
	chatapp::Header header;
	int totalLength = 12 + email.size() + password.size();
	header.set_messagesize(totalLength);
	header.set_messagetype(MessageType::AUTHENTICATE);
	std::string serializedString;
	header.SerializeToString(&serializedString);
	messageBuffer->SetWriteIndex(0);
	messageBuffer->WriteString(serializedString);



	chatapp::AuthenticateWeb authenticateCommand;
	authenticateCommand.set_email(email);
	authenticateCommand.set_password(password);
	authenticateCommand.set_requestid(-1);
	std::string serializedString2;
	authenticateCommand.SerializeToString(&serializedString2);
	messageBuffer->WriteString(serializedString2);

	//header size
	int headerSize = header.ByteSize();
	//actually sending the data
	iResult = send(mConnectSocket, messageBuffer->GetBufferData(), 4, 0);
	iResult = send(mConnectSocket, messageBuffer->GetBufferData() + 4, totalLength, 0);
	if (iResult == SOCKET_ERROR) {
		printf("socket() failed with error: %d\n", iResult);
		closesocket(mConnectSocket);
		WSACleanup();
		connected = false;
	}
}
