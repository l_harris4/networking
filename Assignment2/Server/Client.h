#ifndef _CLIENT_HG_
#define _CLIENT_HG_
#include <vector>
#include <string>
#include <WinSock2.h>

class Client
{
public:
	//ctor
	Client();
	//dtor
	~Client();
	//data members
	std::string userName;
	SOCKET socket;
	//overloading the equality operator
	bool operator==(Client& other);
};

#endif

