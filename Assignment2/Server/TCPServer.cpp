#include "TCPServer.h"
#include "Buffer.h"


static bool deleteAll(SOCKET * theElement) { delete theElement; return true; }

//constructor
TCPServer::TCPServer()
{
	messageBuffer = new Buffer(DEFAULT_BUFFER_LENGTH);
	std::cout << "Server running" << std::endl;
	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;
	connected = true;


	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (connected && iResult != 0) {
		printf("WSAStartup failed: %d\n", iResult);
		connected = false;
	}

	// Socket()
	ListenSocket = socket(AF_INET, SOCK_STREAM, 0);
	if (connected && ListenSocket == INVALID_SOCKET) {
		printf("socket() failed with error %d\n", WSAGetLastError());
		WSACleanup();
		connected = false;
	}

	// Bind()
	iResult = getaddrinfo(NULL, DEFAULT_PORT, &hints, &result);
	iResult = bind(ListenSocket, result->ai_addr, (int)result->ai_addrlen);
	if (connected && iResult == SOCKET_ERROR) {
		printf("bind() failed with error: %d\n", WSAGetLastError());
		freeaddrinfo(result);
		closesocket(ListenSocket);
		WSACleanup();
		connected = false;
	}

	// Listen()
	if (connected && listen(ListenSocket, 5)) {
		printf("listen() failed with error: %d\n", WSAGetLastError());
		closesocket(ListenSocket);
		WSACleanup();
		connected = false;
	}

	ZeroMemory(&hintsServer, sizeof(hintsServer));
	hintsServer.ai_family = AF_UNSPEC;
	hintsServer.ai_socktype = SOCK_STREAM;
	hintsServer.ai_protocol = IPPROTO_TCP;

	//assumed ip of home for this project
	mServerIP = "127.0.0.1";

	iResult = getaddrinfo(mServerIP.c_str(), DEFAULT_PORT_2, &hintsServer, &result);
	if (iResult != 0) {
		printf("getaddrinfo failed with error: %d\n", iResult);
		WSACleanup();
		connected = false;
	}

	ConnectToServer();

}

//destructor
TCPServer::~TCPServer()
{
	for (unsigned threadIndex = 0; threadIndex < threads.size(); ++threadIndex)
	{
		threads[threadIndex]->join();
	}
	delete messageBuffer;
	for (unsigned roomIndex = 0; roomIndex < rooms.size(); roomIndex++)
	{
		for (unsigned clientIndex = 0; clientIndex < rooms[roomIndex].clients.size(); ++clientIndex)
		{
			closesocket(rooms[roomIndex].clients[clientIndex].socket);
		}
	}
	closesocket(ListenSocket);
	WSACleanup();
}

//Name: ConnectToServer
//Purpose: to connect this server to the authentication server
//Accepts: nothing
//Returns: nothing
void TCPServer::ConnectToServer()
{
	for (ptr = result; ptr != NULL; ptr = ptr->ai_next) {
		mConnectSocket = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);
		if (mConnectSocket == INVALID_SOCKET) {
			printf("socket() failed with error: %d\n", iResult);
			WSACleanup();
			connected = false;
		}

		if (connected) {
			iResult = connect(mConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
			if (iResult == SOCKET_ERROR) {
				closesocket(mConnectSocket);
				mConnectSocket = INVALID_SOCKET;
				continue;
			}
		}

		break;
	}

	freeaddrinfo(result);

	if (mConnectSocket == INVALID_SOCKET) {
		printf("Unable to connect to server");
	}
	if (connected)
	{
		std::cout << "Connected to server" << std::endl;
	}
}

//Name: BroadCastClientLeaving
//Purpose: to let other clients know when a client leaves
//Accepts: a client object being the leaving client, and a socket object being the socket of the leaving client
//Returns: nothing
void TCPServer::BroadCastClientLeaving(Client* client, SOCKET clientSocket)
{
	//Broadcast to everyone that this client left the room
	for (int roomIndex = 0; roomIndex < rooms.size(); roomIndex++)
	{
		if (rooms[roomIndex].ClientExistsInRoom((*client)))
		{
			std::string userName = client->userName;
			int eraseIndex = rooms[roomIndex].FindClientInRoom((*client));
			rooms[roomIndex].clients.erase(rooms[roomIndex].clients.begin() + eraseIndex);
			SendMessageToAllClients(userName + " left the room ", roomIndex, clientSocket);
		}
	}
}

//Name: AcceptClient
//Purpose: to accept a client and start up a thread to listen to it
//Accepts: nothing
//Returns: nothing
void TCPServer::AcceptClient()
{
	threads.push_back(new std::thread(&TCPServer::ListenToAuthServer, this, mConnectSocket));
	while (active)
	{
		SOCKET ClientSocket;
		// Accept()
		ClientSocket = accept(ListenSocket, NULL, NULL);
		if (ClientSocket == INVALID_SOCKET)
		{
			printf("accept() failed with error: %d\n", WSAGetLastError());
			closesocket(ListenSocket);
			//WSACleanup();
		}
		else
		{
			std::cout << "Client Connected" << std::endl;
			threads.push_back(new std::thread(&TCPServer::ListenToClient, this, ClientSocket));
		}
	}
}

//Name: ListenToAuthServer
//Purpose: to listen for responses from authentication server
//Accepts: a socket
//Returns: nothing
void TCPServer::ListenToAuthServer(SOCKET authServer)
{
	Client tempClient;
	tempClient.socket = authServer;

	do {
		//take in the header //getting packet length and message id
		iResult = recv(tempClient.socket, messageBuffer->GetBufferData(), 4, 0);

		if (iResult > 0)
		{
			messageBuffer->SetReadIndex(0);
			chatapp::Header deserialized_header;
			deserialized_header.ParseFromString(messageBuffer->ReadString(4));

			int packetLength = deserialized_header.messagesize();
			int message_id = deserialized_header.messagetype();

			switch (message_id)
			{
			case MessageType::REGISTERSUCCESS:
			{
				//might have to do another recv here using the packetLength
				iResult = recv(tempClient.socket, messageBuffer->GetBufferData(), packetLength, 0);
				if (iResult > 0)
				{
					messageBuffer->SetReadIndex(0);
					chatapp::CreateAccountWebSuccess deserializedMessage;
					//maybe the line below should use packetLength
					deserializedMessage.ParseFromString(messageBuffer->ReadString(packetLength));
					int requestID = deserializedMessage.requestid();
					int userID = deserializedMessage.userid();

					chatapp::Header header;
					int totalLength = 8;
					header.set_messagesize(totalLength);
					header.set_messagetype(MessageType::REGISTERSUCCESS);
					std::string serializedString;
					header.SerializeToString(&serializedString);
					messageBuffer->SetWriteIndex(0);
					messageBuffer->WriteString(serializedString);

					std::string serializedString2;
					deserializedMessage.SerializeToString(&serializedString2);
					messageBuffer->WriteString(serializedString2);

					//TODO find the client this belongs to and send that to them
					SOCKET* client = FindClientByPort(requestID);

					iResult = send((*client), messageBuffer->GetBufferData(), 4, 0);
					iResult = send((*client), messageBuffer->GetBufferData() + 4, totalLength, 0);
				}
			}
			break;
			case MessageType::REGISTERFAILURE:
			{
				//might have to do another recv here using the packetLength
				iResult = recv(tempClient.socket, messageBuffer->GetBufferData(), packetLength, 0);
				if (iResult > 0)
				{
					messageBuffer->SetReadIndex(0);
					chatapp::CreateAccountWebFailure deserializedMessage;
					//maybe the line below should use packetLength
					deserializedMessage.ParseFromString(messageBuffer->ReadString(packetLength));
					chatapp::CreateAccountWebFailure_ReasonType reason = deserializedMessage.reason();
					int requestID = deserializedMessage.requestid();

					chatapp::Header header;
					int totalLength = 8;
					header.set_messagesize(totalLength);
					header.set_messagetype(MessageType::REGISTERFAILURE);
					std::string serializedString;
					header.SerializeToString(&serializedString);
					messageBuffer->SetWriteIndex(0);
					messageBuffer->WriteString(serializedString);

					std::string serializedString2;
					deserializedMessage.SerializeToString(&serializedString2);
					messageBuffer->WriteString(serializedString2);

					SOCKET* client = FindClientByPort(requestID);

					iResult = send((*client), messageBuffer->GetBufferData(), 4, 0);
					iResult = send((*client), messageBuffer->GetBufferData() + 4, totalLength, 0);
				}
			}
			break;
			case MessageType::AUTHENTICATESUCCESS:
			{
				//might have to do another recv here using the packetLength
				iResult = recv(tempClient.socket, messageBuffer->GetBufferData(), packetLength, 0);
				if (iResult > 0)
				{
					messageBuffer->SetReadIndex(0);
					chatapp::AuthenticateWebSuccess deserializedMessage;
					//maybe the line below should use packetLength
					deserializedMessage.ParseFromString(messageBuffer->ReadString(packetLength));
					std::string date = deserializedMessage.creationdate();
					int requestID = deserializedMessage.requestid();

					chatapp::Header header;
					int totalLength = 8 + date.size();
					header.set_messagesize(totalLength);
					header.set_messagetype(MessageType::AUTHENTICATESUCCESS);
					std::string serializedString;
					header.SerializeToString(&serializedString);
					messageBuffer->SetWriteIndex(0);
					messageBuffer->WriteString(serializedString);

					std::string serializedString2;
					deserializedMessage.SerializeToString(&serializedString2);
					messageBuffer->WriteString(serializedString2);

					//find the client this belongs to and send that to them
					SOCKET* client = FindClientByPort(requestID);

					iResult = send((*client), messageBuffer->GetBufferData(), 4, 0);
					iResult = send((*client), messageBuffer->GetBufferData() + 4, totalLength, 0);
				}
			}
			break;
			case MessageType::AUTHENTICATEFAILURE:
			{
				//might have to do another recv here using the packetLength
				iResult = recv(tempClient.socket, messageBuffer->GetBufferData(), packetLength, 0);
				if (iResult > 0)
				{
					messageBuffer->SetReadIndex(0);
					chatapp::AuthenticateWebFailure deserializedMessage;
					//maybe the line below should use packetLength
					deserializedMessage.ParseFromString(messageBuffer->ReadString(packetLength));
					chatapp::AuthenticateWebFailure_FailureType reason = deserializedMessage.reason();

					int requestID = deserializedMessage.requestid();

					chatapp::Header header;
					int totalLength = 8;
					header.set_messagesize(totalLength);
					header.set_messagetype(MessageType::AUTHENTICATEFAILURE);
					std::string serializedString;
					header.SerializeToString(&serializedString);
					messageBuffer->SetWriteIndex(0);
					messageBuffer->WriteString(serializedString);

					std::string serializedString2;
					deserializedMessage.SerializeToString(&serializedString2);
					messageBuffer->WriteString(serializedString2);

					SOCKET* client = FindClientByPort(requestID);

					iResult = send((*client), messageBuffer->GetBufferData(), 4, 0);
					iResult = send((*client), messageBuffer->GetBufferData() + 4, totalLength, 0);
				}
			}
			break;
			}
		}


		if (iResult == SOCKET_ERROR) {
			printf("recv failed with error: %d\n", WSAGetLastError());
			closesocket(tempClient.socket);
		}

		if (iResult > 0) {
			printf("Bytes Received: %d\n", iResult);
		}

		if (iResult == 0) {
			printf("Connection closing...\n");
		}

		std::cout << "Data recieved" << std::endl;

	} while (iResult > 0 && active);
}

//Name: ListenToClient
//Purpose: to listen for messages from a specific 
//Accepts: a socket
//Returns: nothing
void TCPServer::ListenToClient(SOCKET clientSocket)
{
	Client tempClient;
	tempClient.socket = clientSocket;

	do {
		//take in the header //getting packet length and message id
		iResult = recv(tempClient.socket, messageBuffer->GetBufferData(), 4, 0);

		if (iResult > 0)
		{
			messageBuffer->SetReadIndex(0);
			chatapp::Header deserialized_header;
			deserialized_header.ParseFromString(messageBuffer->ReadString(4));

			int packetLength = deserialized_header.messagesize();
			int message_id = deserialized_header.messagetype();

			switch (message_id)
			{
				//Leaving a room
			case MessageType::LEAVEROOM:
			{
				//have to do another recv here using the packetLength
				iResult = recv(tempClient.socket, messageBuffer->GetBufferData(), packetLength, 0);
				if (iResult > 0)
				{
					messageBuffer->SetReadIndex(0);
					chatapp::LeaveRoom deserializedLeaveRoom;
					//maybe the line below should use packetLength
					deserializedLeaveRoom.ParseFromString(messageBuffer->ReadString(packetLength));
					std::string roomName = deserializedLeaveRoom.roomname();
					std::string username = deserializedLeaveRoom.username();

					if (RoomNameExists(roomName))
					{
						//remove the client from the room //if they are in the existing room
						int tempRoomIndex = FindRoomByName(roomName);

						if (rooms[tempRoomIndex].ClientExistsInRoom(tempClient))
						{
							int eraseIndex = rooms[tempRoomIndex].FindClientInRoom(tempClient);
							rooms[tempRoomIndex].clients.erase(rooms[tempRoomIndex].clients.begin() + eraseIndex);
							SendMessageToAllClients(username + " left the room ", FindRoomByName(roomName), clientSocket);
						}
					}
				}
				else
				{
					BroadCastClientLeaving(&tempClient, clientSocket);
				}

			}
			break;
			//Joining a room
			case MessageType::JOINROOM:
			{
				//have to do another recv here using the packetLength
				iResult = recv(tempClient.socket, messageBuffer->GetBufferData(), packetLength, 0);
				if (iResult > 0)
				{
					messageBuffer->SetReadIndex(0);
					chatapp::JoinRoom deserializedJoinRoom;
					//maybe the line below should use packetLength
					deserializedJoinRoom.ParseFromString(messageBuffer->ReadString(packetLength));
					std::string roomName = deserializedJoinRoom.roomname();
					std::string username = deserializedJoinRoom.username();
					tempClient.userName = username;


					if (RoomNameExists(roomName))
					{
						//add the client to the existing room //if the client is not already in the room
						Room tempRoom = rooms[FindRoomByName(roomName)];
						if (!tempRoom.ClientExistsInRoom(tempClient))
						{
							rooms[FindRoomByName(roomName)].clients.push_back(tempClient);
							SendMessageToAllClients(username + " joined the room", FindRoomByName(roomName), clientSocket);
						}
					}
					else
					{
						//create the new room using the room name
						Room newRoom;
						newRoom.clients.push_back(tempClient);
						newRoom.roomName = roomName;
						rooms.push_back(newRoom);
						int roomIndex = FindRoomByName(roomName);
						SendMessageToAllClients(username + " joined the room: " + roomName, roomIndex, clientSocket);
						//add the client to the new room
					}
				}
				else
				{
					BroadCastClientLeaving(&tempClient, clientSocket);
				}

			}
			break;
			case MessageType::MESSAGE:
			{
				//might have to do another recv here using the packetLength
				iResult = recv(tempClient.socket, messageBuffer->GetBufferData(), packetLength, 0);
				if (iResult > 0)
				{
					messageBuffer->SetReadIndex(0);
					chatapp::Msg deserializedMessage;
					//maybe the line below should use packetLength
					deserializedMessage.ParseFromString(messageBuffer->ReadString(packetLength));
					std::string roomName = deserializedMessage.roomname();
					std::string message = deserializedMessage.message();

					//send the message out to all the clients
					if (RoomNameExists(roomName) && rooms[FindRoomByName(roomName)].ClientExistsInRoom(tempClient)) {
						SendMessageToAllClients(message + "[" + roomName + "]", FindRoomByName(roomName), clientSocket);
					}
				}
				else
				{
					BroadCastClientLeaving(&tempClient, clientSocket);
				}
			}
			break;
			case MessageType::REGISTER:
			{
				//might have to do another recv here using the packetLength
				iResult = recv(tempClient.socket, messageBuffer->GetBufferData(), packetLength, 0);
				if (iResult > 0)
				{
					messageBuffer->SetReadIndex(0);
					chatapp::CreateAccountWeb deserializedMessage;
					//maybe the line below should use packetLength
					deserializedMessage.ParseFromString(messageBuffer->ReadString(packetLength));
					std::string email = deserializedMessage.email();
					std::string password = deserializedMessage.password();

					try {
							//if nothing went wrong send creation success to client
							chatapp::Header header;
							int totalLength = 12 + email.size() + password.size();
							header.set_messagesize(totalLength);
							header.set_messagetype(MessageType::REGISTER);
							std::string serializedString;
							header.SerializeToString(&serializedString);
							messageBuffer->SetWriteIndex(0);
							messageBuffer->WriteString(serializedString);

							chatapp::CreateAccountWeb authenCommand;
							authenCommand.set_requestid(tempClient.socket);
							authenCommand.set_email(email);
							authenCommand.set_password(password);
							std::string serializedString2;
							authenCommand.SerializeToString(&serializedString2);
							messageBuffer->WriteString(serializedString2);

							int serverResult = 0;

							serverResult = send(mConnectSocket, messageBuffer->GetBufferData(), 4, 0);
							if (serverResult == SOCKET_ERROR || serverResult == 0)
							{
								throw 1;
							}
							serverResult = send(mConnectSocket, messageBuffer->GetBufferData() + 4, totalLength, 0);
							if (serverResult == SOCKET_ERROR || serverResult == 0)
							{
								throw 1;
							}
					}
					catch (...)
					{
						//if some other error occurred along the way, send that to the client
						messageBuffer->SetWriteIndex(0);

						chatapp::Header header;
						int totalLength = 12;
						header.set_messagesize(totalLength);
						header.set_messagetype(MessageType::REGISTERFAILURE);
						std::string serializedString;
						header.SerializeToString(&serializedString);
						messageBuffer->SetWriteIndex(0);
						messageBuffer->WriteString(serializedString);

						chatapp::CreateAccountWebFailure authenCommand;
						authenCommand.set_reason(chatapp::CreateAccountWebFailure_ReasonType::CreateAccountWebFailure_ReasonType_INTERNAL_SERVER_ERROR);
						authenCommand.set_requestid(-1);
						std::string serializedString2;
						authenCommand.SerializeToString(&serializedString2);
						messageBuffer->WriteString(serializedString2);

						iResult = send(clientSocket, messageBuffer->GetBufferData(), 4, 0);
						iResult = send(clientSocket, messageBuffer->GetBufferData() + 4, totalLength, 0);
					}


				}
				else
				{
					BroadCastClientLeaving(&tempClient, clientSocket);
				}
			}
			break;
			case MessageType::AUTHENTICATE:
			{
				//might have to do another recv here using the packetLength
				iResult = recv(tempClient.socket, messageBuffer->GetBufferData(), packetLength, 0);
				if (iResult > 0)
				{
					messageBuffer->SetReadIndex(0);
					chatapp::CreateAccountWeb deserializedMessage;
					//maybe the line below should use packetLength
					deserializedMessage.ParseFromString(messageBuffer->ReadString(packetLength));
					std::string email = deserializedMessage.email();
					std::string password = deserializedMessage.password();

					//if you found stuff here, send authentication success to client
					try {
							messageBuffer->SetWriteIndex(0);

							chatapp::Header header;
							int totalLength = 12 + email.size() + password.size();
							header.set_messagesize(totalLength);
							header.set_messagetype(MessageType::AUTHENTICATE);
							std::string serializedString;
							header.SerializeToString(&serializedString);
							messageBuffer->SetWriteIndex(0);
							messageBuffer->WriteString(serializedString);

							chatapp::AuthenticateWeb authenCommand;
							authenCommand.set_email(email);
							authenCommand.set_password(password);
							authenCommand.set_requestid(tempClient.socket);
							std::string serializedString2;
							authenCommand.SerializeToString(&serializedString2);
							messageBuffer->WriteString(serializedString2);

							int serverResult = 0;

							serverResult = send(mConnectSocket, messageBuffer->GetBufferData(), 4, 0);
							if (serverResult == SOCKET_ERROR || serverResult == 0)
							{
								throw 1;
							}
							serverResult = send(mConnectSocket, messageBuffer->GetBufferData() + 4, totalLength, 0);
							if (serverResult == SOCKET_ERROR || serverResult == 0)
							{
								throw 1;
							}
					}
					catch (...)
					{
						messageBuffer->SetWriteIndex(0);

						chatapp::Header header;
						int totalLength = 12;
						header.set_messagesize(totalLength);
						header.set_messagetype(MessageType::AUTHENTICATEFAILURE);
						std::string serializedString;
						header.SerializeToString(&serializedString);
						messageBuffer->SetWriteIndex(0);
						messageBuffer->WriteString(serializedString);

						//get the type of failure it was
						chatapp::AuthenticateWebFailure authenCommand;
						authenCommand.set_reason(chatapp::AuthenticateWebFailure_FailureType::AuthenticateWebFailure_FailureType_INTERNAL_SERVER_ERROR);
						authenCommand.set_requestid(-1);
						std::string serializedString2;
						authenCommand.SerializeToString(&serializedString2);
						messageBuffer->WriteString(serializedString2);

						iResult = send(clientSocket, messageBuffer->GetBufferData(), 4, 0);
						iResult = send(clientSocket, messageBuffer->GetBufferData() + 4, totalLength, 0);
					}




				}
				else
				{
					BroadCastClientLeaving(&tempClient, clientSocket);
				}
			}
			break;

			}
		}
		else
		{
			BroadCastClientLeaving(&tempClient, clientSocket);
		}


		if (iResult == SOCKET_ERROR) {
			printf("recv failed with error: %d\n", WSAGetLastError());
			closesocket(tempClient.socket);
		}

		if (iResult > 0) {
			printf("Bytes Received: %d\n", iResult);
		}

		if (iResult == 0) {
			printf("Connection closing...\n");
		}

		std::cout << "Data recieved" << std::endl;

	} while (iResult > 0 && active);
}

//Name: SendMessageToAllClients
//Purpose: to send a message to all clients
//Accepts: a string representing the message to send, the index of the room, and the socket respresenting
//         the sender of the message
//Returns: nothing
void TCPServer::SendMessageToAllClients(std::string message, int roomIndex, SOCKET senderSocket)
{
	messageBuffer->SetWriteIndex(0);

	chatapp::Header header;
	int totalLength = 8 + rooms[roomIndex].roomName.size() + message.size();
	header.set_messagesize(totalLength);
	header.set_messagetype(MessageType::MESSAGE);
	std::string serializedString;
	header.SerializeToString(&serializedString);
	messageBuffer->SetWriteIndex(0);
	messageBuffer->WriteString(serializedString);

	chatapp::Msg messageCommand;
	messageCommand.set_roomname(rooms[roomIndex].roomName);
	messageCommand.set_message(message);
	std::string serializedString2;
	messageCommand.SerializeToString(&serializedString2);
	messageBuffer->WriteString(serializedString2);


	int clientsSize = rooms[roomIndex].clients.size();
	for (int clientIndex = 0; clientIndex < clientsSize; ++clientIndex)
	{
		if (rooms[roomIndex].clients[clientIndex].socket != senderSocket) {
			//actually sending the data
			iResult = send(rooms[roomIndex].clients[clientIndex].socket, messageBuffer->GetBufferData(), 4, 0);
			iResult = send(rooms[roomIndex].clients[clientIndex].socket, messageBuffer->GetBufferData() + 4, message.size() + 8 + rooms[roomIndex].roomName.size(), 0);
			if (iResult == SOCKET_ERROR) {
				printf("socket() failed with error: %d\n", iResult);
				closesocket(rooms[roomIndex].clients[clientIndex].socket);
			}
		}
	}
	printf("Data sent");
}

//Name: RoomNameExists
//Purpose: to check if a room exists
//Accepts: the room name as a string
//Returns: a bool saying if the room exists or not
bool TCPServer::RoomNameExists(std::string roomName)
{
	for (unsigned roomIndex = 0; roomIndex < rooms.size(); roomIndex++)
	{
		if (rooms[roomIndex].roomName == roomName)
			return true;
	}
	return false;
}

//Name: FindRoomByName
//Purpose: to find a room by name
//Accepts: the room name as a string
//Returns: an int representing the index of the room
int TCPServer::FindRoomByName(std::string roomName)
{
	for (unsigned roomIndex = 0; roomIndex < rooms.size(); roomIndex++)
	{
		if (rooms[roomIndex].roomName == roomName)
			return roomIndex;
	}
	return -1;
}

//Name: FindClientByPort
//Purpose: to find a client using a certain port on this server
//Accepts: an int being the port number
//Returns: a socket pointer representing the client to send a message to
SOCKET * TCPServer::FindClientByPort(int portNumber)
{
	std::cout << "Found a client by port" << std::endl;
	for (unsigned roomIndex = 0; roomIndex < rooms.size(); roomIndex++)
	{
		for (unsigned clientIndex = 0; clientIndex < rooms[roomIndex].clients.size(); clientIndex++)
		{
			if (rooms[roomIndex].clients[clientIndex].socket == portNumber)
			{
				return &rooms[roomIndex].clients[clientIndex].socket;
			}
		}
	}
	return nullptr;
}
