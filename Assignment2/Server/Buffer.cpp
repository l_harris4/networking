#include "Buffer.h"

//constructor
Buffer::Buffer(size_t size)
	: mWriteIndex(0)
	, mReadIndex(0)
{
	mBuffer.resize(size);
}

//Name: WriteInt32LE
//Purpose: serializing an int into bytes and putting that into the buffer, starting at a certain
//         write index
//Accepts: A size_t representing where to start writting the int in the buffer
//		   and an int representing the value to be serialized
//Returns: nothing
void Buffer::WriteInt32LE(size_t index, int value)
{
	if (mWriteIndex + 4 > mBuffer.size()) {
		mBuffer.resize(mBuffer.size() + 4);
	}
	mWriteIndex = index;
	mBuffer[mWriteIndex++] = value;
	mBuffer[mWriteIndex++] = value >> 8;
	mBuffer[mWriteIndex++] = value >> 16;
	mBuffer[mWriteIndex++] = value >> 24;
}

//Name: WriteInt32LE
//Purpose: serializing an int into bytes and putting that into the buffer
//Accepts: An int representing the value to be serialized
//Returns: nothing
void Buffer::WriteInt32LE(int value)
{
	if (mWriteIndex + 4 > mBuffer.size()) {
		mBuffer.resize(mBuffer.size() + 4);
	}
	mBuffer[mWriteIndex++] = value;
	mBuffer[mWriteIndex++] = value >> 8;
	mBuffer[mWriteIndex++] = value >> 16;
	mBuffer[mWriteIndex++] = value >> 24;
}

//Name: ReadInt32LE
//Purpose: deserializing an int out of the buffer
//Accepts: A size_t representing where to start reading from in the buffer
//Returns: the int value
int Buffer::ReadInt32LE(size_t index)
{
	mReadIndex = index;
	uint32_t value = mBuffer[mReadIndex++];
	value |= mBuffer[mReadIndex++] << 8;
	value |= mBuffer[mReadIndex++] << 16;
	value |= mBuffer[mReadIndex++] << 24;
	return value;
}

//Name: ReadInt32LE
//Purpose: deserializing an int out of the buffer
//Accepts: nothing
//Returns: the int value
int Buffer::ReadInt32LE(void)
{
	uint32_t value = mBuffer[mReadIndex++];
	value |= mBuffer[mReadIndex++] << 8;
	value |= mBuffer[mReadIndex++] << 16;
	value |= mBuffer[mReadIndex++] << 24;
	return value;
}

//Name: WriteInt32BE
//Purpose: serializing an int into bytes and putting that into the buffer, starting at a certain
//         write index
//Accepts: A size_t representing where to start writting the int in the buffer
//		   and an int representing the value to be serialized
//Returns: nothing
void Buffer::WriteInt32BE(size_t index, int value)
{
	if (mWriteIndex + 4 > mBuffer.size()) {
		mBuffer.resize(mBuffer.size() + 4);
	}
	mWriteIndex = index;
	mBuffer[mWriteIndex++] = value >> 24;
	mBuffer[mWriteIndex++] = value >> 16;
	mBuffer[mWriteIndex++] = value >> 8;
	mBuffer[mWriteIndex++] = value;	
}

//Name: WriteInt32BE
//Purpose: serializing an int into bytes and putting that into the buffer
//Accepts: An int representing the value to be serialized
//Returns: nothing
void Buffer::WriteInt32BE(int value)
{
	//if the size is equal to zero it means we have lost connection to the server
	if (mBuffer.size() != 0) 
	{
		if (mWriteIndex + 4 > mBuffer.size()) {
			mBuffer.resize(mBuffer.size() + 4);
		}
		mBuffer[mWriteIndex++] = value >> 24;
		mBuffer[mWriteIndex++] = value >> 16;
		mBuffer[mWriteIndex++] = value >> 8;
		mBuffer[mWriteIndex++] = value;
	}
}

//Name: ReadInt32BE
//Purpose: deserializing an int out of the buffer
//Accepts: A size_t representing where to start reading from in the buffer
//Returns: the int value
int Buffer::ReadInt32BE(size_t index)
{
	mReadIndex = index;
	uint32_t value = mBuffer[mReadIndex++] << 24;
	value |= mBuffer[mReadIndex++] << 16;
	value |= mBuffer[mReadIndex++] << 8;
	value |= mBuffer[mReadIndex++];
	return value;
}

//Name: ReadInt32BE
//Purpose: deserializing an int out of the buffer
//Accepts: nothing
//Returns: the int value
int Buffer::ReadInt32BE(void)
{
	uint32_t value = mBuffer[mReadIndex++] << 24;
	value |= mBuffer[mReadIndex++] << 16;
	value |= mBuffer[mReadIndex++] << 8;
	value |= mBuffer[mReadIndex++];
	return value;
}

//Name: WriteShort16LE
//Purpose: serializing an int into bytes and putting that into the buffer, starting at a certain
//         write index
//Accepts: A size_t representing where to start writing the short in the buffer
//		   and a short representing the value to be serialized
//Returns: nothing
void Buffer::WriteShort16LE(size_t index, int value)
{
	if (mWriteIndex + 2 > mBuffer.size()) {
		mBuffer.resize(mBuffer.size() + 2);
	}
	mWriteIndex = index;
	mBuffer[mWriteIndex++] = value;
	mBuffer[mWriteIndex++] = value >> 8;
}

//Name: WriteShort16LE
//Purpose: serializing an int into bytes and putting that into the buffer
//Accepts: An int representing the value to be serialized
//Returns: nothing
void Buffer::WriteShort16LE(int value)
{
	if (mWriteIndex + 2 > mBuffer.size()) {
		mBuffer.resize(mBuffer.size() + 2);
	}
	mBuffer[mWriteIndex++] = value;
	mBuffer[mWriteIndex++] = value >> 8;
}

//Name: WriteShort16BE
//Purpose: serializing a short into bytes and putting that into the buffer, starting at a certain
//         write index
//Accepts: A size_t representing where to start writing the short in the buffer
//		   and a short representing the value to be serialized
//Returns: nothing
void Buffer::WriteShort16BE(size_t index, int value)
{
	if (mWriteIndex + 2 > mBuffer.size()) {
		mBuffer.resize(mBuffer.size() + 2);
	}
	mWriteIndex = index;
	mBuffer[mWriteIndex++] = value >> 8;
	mBuffer[mWriteIndex++] = value;
}

//Name: WriteShort16BE
//Purpose: serializing a short into bytes and putting that into the buffer
//Accepts: An int representing the value to be serialized
//Returns: nothing
void Buffer::WriteShort16BE(int value)
{
	if (mWriteIndex + 2 > mBuffer.size()) {
		mBuffer.resize(mBuffer.size() + 2);
	}
	mBuffer[mWriteIndex++] = value >> 8;
	mBuffer[mWriteIndex++] = value;
}


//Name: ReadShort16LE
//Purpose: deserializing a short out of the buffer
//Accepts: A size_t representing where to start reading from in the buffer
//Returns: the short value
short Buffer::ReadShort16LE(size_t index)
{
	mReadIndex = index;
	short value = mBuffer[mReadIndex++];
	value |= mBuffer[mReadIndex++] << 8;
	return value;
}

//Name: ReadShort16LE
//Purpose: deserializing a short out of the buffer
//Accepts: nothing
//Returns: the short value
short Buffer::ReadShort16LE(void)
{
	short value = mBuffer[mReadIndex++];
	value |= mBuffer[mReadIndex++] << 8;
	return value;
}

//Name: ReadShort16BE
//Purpose: deserializing a short out of the buffer
//Accepts: A size_t representing where to start reading from in the buffer
//Returns: the short value
short Buffer::ReadShort16BE(size_t index)
{
	mReadIndex = index;
	short value = mBuffer[mReadIndex++] << 8;
	value |= mBuffer[mReadIndex++];
	return value;
}

//Name: ReadShort16BE
//Purpose: deserializing a short out of the buffer
//Accepts: nothing
//Returns: the short value
short Buffer::ReadShort16BE(void)
{
	short value = mBuffer[mReadIndex++] << 8;
	value |= mBuffer[mReadIndex++];
	return value;
}


//Name: WriteString
//Purpose: serializing a string into bytes and putting that into the buffer, starting at a certain
//         write index
//Accepts: A size_t representing where to start writing the short in the buffer
//		   and a string representing the value to be serialized
//Returns: nothing
void Buffer::WriteString(size_t index, std::string value)
{
	//if the size is equal to zero it means we have lost connection to the server
	if (mBuffer.size() != 0)
	{
		mWriteIndex = index;
		if (mWriteIndex + value.size() + 1 > mBuffer.size()) {
			mBuffer.resize(mBuffer.size() + value.size() + 1);
		}
		for (unsigned stringIndex = 0; stringIndex < value.size(); stringIndex++)
		{
			mBuffer[mWriteIndex++] = value[stringIndex];
		}
	}

}

//Name: WriteString
//Purpose: serializing a string into bytes and putting that into the buffer
//Accepts: An string representing the value to be serialized
//Returns: nothing
void Buffer::WriteString(std::string value)
{
	//if the size is equal to zero it means we have lost connection to the server
	if (mBuffer.size() != 0)
	{
		if (mWriteIndex + value.size() + 1 > mBuffer.size()) {
			mBuffer.resize(mBuffer.size() + value.size() + 1);
		}
		for (unsigned stringIndex = 0; stringIndex < value.size(); stringIndex++)
		{
			mBuffer[mWriteIndex++] = value[stringIndex];
		}
	}
}

//Name: ReadString
//Purpose: deserializing a string out of the buffer
//Accepts: A size_t representing where to start reading from in the buffer
//		   an int representing the number of chars in the string
//Returns: the string value
std::string Buffer::ReadString(size_t index, int size)
{
	std::string returnString = "";
	mReadIndex = index;
	for (int stringIndex = 0; stringIndex < size; stringIndex++)
	{
		returnString += mBuffer[mReadIndex++];
	}
	return returnString;
}

//Name: ReadString
//Purpose: deserializing a short out of the buffer
//Accepts: an int representing the size of the string
//Returns: the short value
std::string Buffer::ReadString(int size)
{
	std::string returnString = "";
	for (int stringIndex = 0; stringIndex < size; stringIndex++)
	{
		returnString += mBuffer[mReadIndex++];
	}
	return returnString;
}

//Name: SetWriteIndex
//Purpose: to set the write index of the buffer
//Accepts: an unsigned int representing the new index
//Returns: nothing
void Buffer::SetWriteIndex(unsigned int index)
{
	this->mWriteIndex = index;
}

//Name: SetReadIndex
//Purpose: to set the read index of the buffer
//Accepts: an unsigned int representing the new index
//Returns: nothing
void Buffer::SetReadIndex(unsigned int index)
{
	this->mReadIndex = index;
}

//Name: GetReadIndex
//Purpose: to get the read index of the buffer
//Accepts: nothing
//Returns: an unsigned int representing the read index
unsigned int Buffer::GetReadIndex()
{
	return this->mReadIndex;
}

//Name: GetWriteIndex
//Purpose: to get the write index of the buffer
//Accepts: nothing
//Returns: an unsigned int representing the write index
unsigned int Buffer::GetWriteIndex()
{
	return this->mWriteIndex;
}


//Name: GetBufferData
//Purpose: to retrieve the buffer data
//Accepts: nothing
//Returns: a char* representing the buffer data
char* Buffer::GetBufferData()
{
	return this->mBuffer.data();
}

//Name: ResizeBuffer
//Purpose: to resize the buffer
//Accepts: int size
//Returns: nothing
void Buffer::ResizeBuffer(int size)
{
	mBuffer.resize(size);
}

//Name: GetBufferSize
//Purpose: to retrieve the buffer size
//Accepts: nothing
//Returns: an int representing the buffer size
int Buffer::GetBufferSize()
{
	return this->mBuffer.size();
}



