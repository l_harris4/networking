#include "TCPServer.h"
#include "Buffer.h"

static bool deleteAll(SOCKET * theElement) { delete theElement; return true; }

//constructor
TCPServer::TCPServer()
{
	messageBuffer = new Buffer(DEFAULT_BUFFER_LENGTH);
	std::cout << "Server running" << std::endl;
	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;
	connected = true;


	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (connected && iResult != 0) {
		printf("WSAStartup failed: %d\n", iResult);
		connected = false;
	}

	// Socket()
	ListenSocket = socket(AF_INET, SOCK_STREAM, 0);
	if (connected && ListenSocket == INVALID_SOCKET) {
		printf("socket() failed with error %d\n", WSAGetLastError());
		WSACleanup();
		connected = false;
	}

	// Bind()
	iResult = getaddrinfo(NULL, DEFAULT_PORT, &hints, &result);
	iResult = bind(ListenSocket, result->ai_addr, (int)result->ai_addrlen);
	if (connected && iResult == SOCKET_ERROR) {
		printf("bind() failed with error: %d\n", WSAGetLastError());
		freeaddrinfo(result);
		closesocket(ListenSocket);
		WSACleanup();
		connected = false;
	}

	// Listen()
	if (connected && listen(ListenSocket, 5)) {
		printf("listen() failed with error: %d\n", WSAGetLastError());
		closesocket(ListenSocket);
		WSACleanup();
		connected = false;
	}

}

//destructor
TCPServer::~TCPServer()
{
	for (unsigned threadIndex = 0; threadIndex < threads.size(); ++threadIndex)
	{
		threads[threadIndex]->join();
	}
	delete messageBuffer;
	for (unsigned roomIndex = 0; roomIndex < rooms.size(); roomIndex++)
	{
		for (unsigned clientIndex = 0; clientIndex < rooms[roomIndex].clients.size(); ++clientIndex)
		{
			closesocket(rooms[roomIndex].clients[clientIndex].socket);
		}
	}
	/*for (int clientIndex = 0; clientIndex < clientsSize; ++clientIndex)
	{
		closesocket(clients[clientIndex]);
	}*/
	closesocket(ListenSocket);
	WSACleanup();
}

void TCPServer::AcceptClient()
{
	while (active)
	{
		SOCKET ClientSocket;
		// Accept()
		ClientSocket = accept(ListenSocket, NULL, NULL);
		if (ClientSocket == INVALID_SOCKET) 
		{
			printf("accept() failed with error: %d\n", WSAGetLastError());
			closesocket(ListenSocket);
			//WSACleanup();
		}
		else
		{
			std::cout << "Client Connected" << std::endl;
			threads.push_back(new std::thread(&TCPServer::ListenToClient, this, ClientSocket));
		}
	}
}

//Name: ListenToClient
//Purpose: to listen for messages from a specific 
//Accepts: a socket
//Returns: nothing
void TCPServer::ListenToClient(SOCKET clientSocket)
{
	Client tempClient;
	tempClient.socket = clientSocket;

	do {
		//take in the header //getting packet length and message id
		iResult = recv(tempClient.socket, messageBuffer->GetBufferData(), sizeof(uint32_t) * 2, 0);

		if (iResult > 0)
		{
			messageBuffer->SetReadIndex(0);

			int packetLength = messageBuffer->ReadInt32BE();
			int message_id = messageBuffer->ReadInt32BE();

			if (packetLength > messageBuffer->GetBufferSize()) {
				messageBuffer->ResizeBuffer(packetLength);
			}

			switch (message_id)
			{
				//Leaving a room
			case MessageType::LEAVEROOM:
			{
				//have to do another recv here using the packetLength
				iResult = recv(tempClient.socket, messageBuffer->GetBufferData(), packetLength, 0);
				if (iResult > 0)
				{
					messageBuffer->SetReadIndex(0);
					//get the room name
					std::string roomName;
					std::string username;
					int roomToJoinLength = messageBuffer->ReadInt32BE();
					roomName = messageBuffer->ReadString(roomToJoinLength);

					//get the username
					int nameLength = messageBuffer->ReadInt32BE();
					username = messageBuffer->ReadString(nameLength);


					if (RoomNameExists(roomName))
					{
						//remove the client from the room //if they are in the existing room
						int tempRoomIndex = FindRoomByName(roomName);

						if (rooms[tempRoomIndex].ClientExistsInRoom(tempClient))
						{
							int eraseIndex = rooms[tempRoomIndex].FindClientInRoom(tempClient);
							rooms[tempRoomIndex].clients.erase(rooms[tempRoomIndex].clients.begin() + eraseIndex);
							SendMessageToAllClients(username + " left the room ", FindRoomByName(roomName), clientSocket);
						}
					}
				}
				else
				{
					//Broadcast to everyone that this client left the room
					for (int roomIndex = 0; roomIndex < rooms.size(); roomIndex++)
					{
						if (rooms[roomIndex].ClientExistsInRoom(tempClient))
						{
							std::string userName = tempClient.userName;
							int eraseIndex = rooms[roomIndex].FindClientInRoom(tempClient);
							rooms[roomIndex].clients.erase(rooms[roomIndex].clients.begin() + eraseIndex);
							SendMessageToAllClients(userName + " left the room ", roomIndex, clientSocket);
						}
					}
				}

			}
			break;
			//Joining a room
			case MessageType::JOINROOM:
			{
				//have to do another recv here using the packetLength
				iResult = recv(tempClient.socket, messageBuffer->GetBufferData(), packetLength, 0);
				if (iResult > 0)
				{
					messageBuffer->SetReadIndex(0);
					//get the room name
					std::string roomName;
					std::string username;
					int roomToJoinLength = messageBuffer->ReadInt32BE();
					roomName = messageBuffer->ReadString(roomToJoinLength);

					//get the username
					int nameLength = messageBuffer->ReadInt32BE();
					username = messageBuffer->ReadString(nameLength);
					tempClient.userName = username;


					if (RoomNameExists(roomName))
					{
						//add the client to the existing room //if the client is not already in the room
						Room tempRoom = rooms[FindRoomByName(roomName)];
						if (!tempRoom.ClientExistsInRoom(tempClient))
						{
							rooms[FindRoomByName(roomName)].clients.push_back(tempClient);
							SendMessageToAllClients(username + " joined the room", FindRoomByName(roomName), clientSocket);
						}
					}
					else
					{
						//create the new room using the room name
						Room newRoom;
						newRoom.clients.push_back(tempClient);
						newRoom.roomName = roomName;
						rooms.push_back(newRoom);
						int roomIndex = FindRoomByName(roomName);
						SendMessageToAllClients(username + " joined the room: " + roomName, roomIndex, clientSocket);
						//add the client to the new room
					}
				}
				else
				{
					//Broadcast to everyone that this client left the room
					for (int roomIndex = 0; roomIndex < rooms.size(); roomIndex++)
					{
						if (rooms[roomIndex].ClientExistsInRoom(tempClient))
						{
							std::string userName = tempClient.userName;
							int eraseIndex = rooms[roomIndex].FindClientInRoom(tempClient);
							rooms[roomIndex].clients.erase(rooms[roomIndex].clients.begin() + eraseIndex);
							SendMessageToAllClients(userName + " left the room ", roomIndex, clientSocket);
						}
					}	
				}

			}
			break;
			case MessageType::MESSAGE:
			{
				//might have to do another recv here using the packetLength
				iResult = recv(tempClient.socket, messageBuffer->GetBufferData(), packetLength, 0);
				if (iResult > 0)
				{
					messageBuffer->SetReadIndex(0);
					std::string roomName;
					//get the room name
					int roomNameLength = messageBuffer->ReadInt32BE();
					roomName = messageBuffer->ReadString(roomNameLength);

					//get the message
					int messageLength = messageBuffer->ReadInt32BE();
					std::string message = messageBuffer->ReadString(messageLength);

					//send the message out to all the clients
					if (RoomNameExists(roomName) && rooms[FindRoomByName(roomName)].ClientExistsInRoom(tempClient)) {
						SendMessageToAllClients(message + "[" + roomName + "]", FindRoomByName(roomName), clientSocket);
					}
				}
				else
				{
					//Broadcast to everyone that this client left the room
					for (int roomIndex = 0; roomIndex < rooms.size(); roomIndex++)
					{
						if (rooms[roomIndex].ClientExistsInRoom(tempClient))
						{
							std::string userName = tempClient.userName;
							int eraseIndex = rooms[roomIndex].FindClientInRoom(tempClient);
							rooms[roomIndex].clients.erase(rooms[roomIndex].clients.begin() + eraseIndex);
							SendMessageToAllClients(userName + " left the room ", roomIndex, clientSocket);
						}
					}
				}
			}
			break;

			}
		}
		else
		{
			//Broadcast to everyone that this client left the room
			for (int roomIndex = 0; roomIndex < rooms.size(); roomIndex++)
			{
				if (rooms[roomIndex].ClientExistsInRoom(tempClient))
				{
					std::string userName = tempClient.userName;
					int eraseIndex = rooms[roomIndex].FindClientInRoom(tempClient);
					rooms[roomIndex].clients.erase(rooms[roomIndex].clients.begin() + eraseIndex);
					SendMessageToAllClients(userName + " left the room ", roomIndex, clientSocket);
				}
			}
		}


		if (iResult == SOCKET_ERROR) {
			printf("recv failed with error: %d\n", WSAGetLastError());
			closesocket(tempClient.socket);
		}

		if (iResult > 0) {
			printf("Bytes Received: %d\n", iResult);
		}

		if (iResult == 0) {
			printf("Connection closing...\n");
		}

		std::cout << "Data recieved" << std::endl;

	} while (iResult > 0 && active);
}

//Name: SendMessageToAllClients
//Purpose: to send a message to all clients
//Accepts: a string representing the message to send, the index of the room, and the socket respresenting
//         the sender of the message
//Returns: nothing
void TCPServer::SendMessageToAllClients(std::string message, int roomIndex, SOCKET senderSocket)
{
	messageBuffer->SetWriteIndex(0);
	//build a header to send that
	//serialize the total length
	int packetLength = 8 + message.size() + rooms[roomIndex].roomName.size();
	messageBuffer->WriteInt32BE(packetLength);

	//serialize the message id
	messageBuffer->WriteInt32BE(MessageType::MESSAGE);


	//length of roomName
	messageBuffer->WriteInt32BE(rooms[roomIndex].roomName.size());
	//roomName
	messageBuffer->WriteString(rooms[roomIndex].roomName);
	//length of message
	messageBuffer->WriteInt32BE(message.size());
	//message
	messageBuffer->WriteString(message);


	int clientsSize = rooms[roomIndex].clients.size();
	for (int clientIndex = 0; clientIndex < clientsSize; ++clientIndex)
	{
		if (rooms[roomIndex].clients[clientIndex].socket != senderSocket) {
			//actually sending the data
			iResult = send(rooms[roomIndex].clients[clientIndex].socket, messageBuffer->GetBufferData(), 8, 0);
			iResult = send(rooms[roomIndex].clients[clientIndex].socket, messageBuffer->GetBufferData() + 8, message.size() + 8 + rooms[roomIndex].roomName.size(), 0);
			if (iResult == SOCKET_ERROR) {
				printf("socket() failed with error: %d\n", iResult);
				closesocket(rooms[roomIndex].clients[clientIndex].socket);
			}
		}
	}
	printf("Data sent");
}

//Name: RoomNameExists
//Purpose: to check if a room exists
//Accepts: the room name as a string
//Returns: a bool saying if the room exists or not
bool TCPServer::RoomNameExists(std::string roomName)
{
	for (unsigned roomIndex = 0; roomIndex < rooms.size(); roomIndex++)
	{
		if (rooms[roomIndex].roomName == roomName)
			return true;
	}
	return false;
}

//Name: FindRoomByName
//Purpose: to find a room by name
//Accepts: the room name as a string
//Returns: an int representing the index of the room
int TCPServer::FindRoomByName(std::string roomName)
{
	for (unsigned roomIndex = 0; roomIndex < rooms.size(); roomIndex++)
	{
		if (rooms[roomIndex].roomName == roomName)
			return roomIndex;
	}
	return -1;
}
