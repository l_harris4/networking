#include "TCPServer.h"

using namespace std;

int main() {
	TCPServer server;

	thread acceptClientsThread (&TCPServer::AcceptClient,server);
	string test = "";

	while (test != "q")
	{
		cin >> test;
		if (test == "q")
		{
			acceptClientsThread.join();
			server.active = false;

		}
	}
	system("pause");
}