#ifndef _ROOM_HG_
#define _ROOM_HG_

#include <vector>
#include <string>
#include "Client.h"

class Room
{
public:
	Room();
	~Room();
	std::vector<Client> clients;
	std::string roomName;
	bool ClientExistsInRoom(Client& client);
	int FindClientInRoom(Client& client);
};

#endif
