#ifndef _BUFFER_HG_
#define _BUFFER_HG_

#include <vector>
#include <string>

class Buffer {
public:
	//ctor
	Buffer(size_t size);
	typedef uint32_t size_type;
	void WriteInt32LE(size_t index, int value);
	void WriteInt32LE(int value);

	int ReadInt32LE(size_t index);
	int ReadInt32LE(void);

	void WriteInt32BE(size_t index, int value);
	void WriteInt32BE(int value);

	int ReadInt32BE(size_t index);
	int ReadInt32BE(void);

	void WriteShort16LE(size_t index, int value);
	void WriteShort16LE(int value);

	void WriteShort16BE(size_t index, int value);
	void WriteShort16BE(int value);

	short ReadShort16LE(size_t index);
	short ReadShort16LE(void);

	short ReadShort16BE(size_t index);
	short ReadShort16BE(void);

	void WriteString(size_t index, std::string value);
	void WriteString(std::string value);

	std::string ReadString(size_t index, int size);
	std::string ReadString(int size);

	void SetWriteIndex(unsigned int index);
	unsigned int GetWriteIndex();

	void SetReadIndex(unsigned int index);
	unsigned int GetReadIndex();

	char* GetBufferData();

	void ResizeBuffer(int size);

	int GetBufferSize();

private:
	std::vector<char> mBuffer;
	unsigned int mReadIndex;
	unsigned int mWriteIndex;
};

#endif //_BUFFER_HG_

