#ifndef  _TCPSERVER_HG_
#define _TCPSERVER_HG_
#define WIN32_LEAN_AND_MEAN

#include <Windows.h>
#include <WinSock2.h>
#include <WS2tcpip.h>

#include <iostream>
#include <string>
#include <vector>
#include <thread>

#include "Buffer.h"
#include "Room.h"

#pragma comment(lib, "Ws2_32.lib")

#define DEFAULT_PORT "5000"
#define DEFAULT_BUFFER_LENGTH 512



class TCPServer
{
public:
	TCPServer();
	~TCPServer();
private:
	WSADATA wsaData;
	int iResult;
	//char recvbuf[DEFAULT_BUFFER_LENGTH];
	//int recvbuflen = DEFAULT_BUFFER_LENGTH;
	SOCKET ListenSocket;
	std::vector<Room> rooms;
	std::vector<std::thread*> threads;
	struct addrinfo* result = 0;
	struct addrinfo hints;
	bool connected;
	Buffer* messageBuffer;
	//SOCKET ClientSocket;
public:
	void AcceptClient();
	bool active = true;
	void ListenToClient(SOCKET clientSocket);
	void SendMessageToAllClients(std::string message, int roomIndex, SOCKET senderSocket);
	bool RoomNameExists(std::string roomName);
	int FindRoomByName(std::string roomName);
};

enum MessageType
{
	MESSAGE = 0,
	JOINROOM,
	LEAVEROOM
};

#endif // ! _TCPSERVER_HG_

