#include "Client.h"

//constructor
Client::Client()
{
}

//destructor
Client::~Client()
{
}

//overloading the equality operator for comparision of Clients
bool Client::operator==(Client & other)
{
	return this->userName == other.userName && this->socket == other.socket;
}
