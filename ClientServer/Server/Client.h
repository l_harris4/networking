#ifndef _CLIENT_HG_
#define _CLIENT_HG_
#include <vector>
#include <string>
#include <WinSock2.h>

class Client
{
public:
	Client();
	~Client();
	std::string userName;
	SOCKET socket;
	bool operator==(Client& other);
};

#endif

