#include "TCPClient.h"

//constructor
TCPClient::TCPClient()
{
	roomNameIndex = 0;
	messageBuffer = new Buffer(DEFAULT_BUFFER_LENGTH);

	std::cout << "Client running" << std::endl;
	connected = true;

	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &mWsaData);
	if (iResult != 0) {
		printf("WSAStartup failed: %d\n", iResult);
		connected = false;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	//assumed ip of home for this project
	mServerIP = "127.0.0.1";

	iResult = getaddrinfo(mServerIP.c_str(), DEFAULT_PORT, &hints, &result);
	if (iResult != 0) {
		printf("getaddrinfo failed with error: %d\n", iResult);
		WSACleanup();
		connected = false;
	}

}

//destructor
TCPClient::~TCPClient()
{
	delete messageBuffer;
	iResult = shutdown(mConnectSocket, SD_SEND);
	if (iResult == SOCKET_ERROR) {
		printf("shutdown() failed with error: %d\n", iResult);
		closesocket(mConnectSocket);
		WSACleanup();
		connected = false;
	}

	closesocket(mConnectSocket);
	WSACleanup();
}

//Name: ConnectToServer
//Purpose: to connect the client to the server
//Accepts: nothing
//Returns: nothing
void TCPClient::ConnectToServer()
{
	for (ptr = result; ptr != NULL; ptr = ptr->ai_next) {
		mConnectSocket = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);
		if (mConnectSocket == INVALID_SOCKET) {
			printf("socket() failed with error: %d\n", iResult);
			WSACleanup();
			connected = false;
		}

		if (connected) {
			iResult = connect(mConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
			if (iResult == SOCKET_ERROR) {
				closesocket(mConnectSocket);
				mConnectSocket = INVALID_SOCKET;
				continue;
			}
		}

		break;
	}

	freeaddrinfo(result);

	if (mConnectSocket == INVALID_SOCKET) {
		printf("Unable to connect to server");
		WSACleanup();
		connected = false;
	}
	if (connected)
	{
		std::cout << "Connected to server" << std::endl;
	}
}

//Name: JoinRoom
//Purpose: to join the client to a room on the server
//Accepts: a string representing the room name that the client wants to join
//Returns: nothing
void TCPClient::JoinRoom(std::string roomName)
{
	//send username and room you want to join

	messageBuffer->SetWriteIndex(0);
	//serialize the total length
	int totalLength = 8 + roomName.size() + username.size();
	messageBuffer->WriteInt32BE(totalLength);
	//serialize the message id
	messageBuffer->WriteInt32BE(MessageType::JOINROOM);

	//serialize room name length
	messageBuffer->WriteInt32BE(roomName.size());
	//serialize room name
	messageBuffer->WriteString(roomName);
	//serialize the username length
	messageBuffer->WriteInt32BE(username.size());
	//serialize the username
	messageBuffer->WriteString(username);

	//actually sending the data
	iResult = send(mConnectSocket, messageBuffer->GetBufferData(), sizeof(uint32_t) * 2, 0);
	iResult = send(mConnectSocket, messageBuffer->GetBufferData() + 8, totalLength, 0);
	if (iResult == SOCKET_ERROR) {
		printf("socket() failed with error: %d\n", iResult);
		closesocket(mConnectSocket);
		WSACleanup();
		connected = false;
	}
	InsertRoomName(roomName);
}

//Name: LeaveRoom
//Purpose: to take this client out of a room on the server
//Accepts: a string representing the room name that the client wants to leave
//Returns: nothing
void TCPClient::LeaveRoom(std::string roomName)
{
	//connectToServer();
	//send username and room you want to join

	messageBuffer->SetWriteIndex(0);
	//serialize the total length
	int totalLength = 8 + roomName.size() + username.size();
	messageBuffer->WriteInt32BE(totalLength);
	//serialize the message id
	messageBuffer->WriteInt32BE(MessageType::LEAVEROOM);

	//serialize room name length
	messageBuffer->WriteInt32BE(roomName.size());
	//serialize room name
	messageBuffer->WriteString(roomName);
	//serialize the username length
	messageBuffer->WriteInt32BE(username.size());
	//serialize the username
	messageBuffer->WriteString(username);

	//actually sending the data
	iResult = send(mConnectSocket, messageBuffer->GetBufferData(), sizeof(uint32_t) * 2, 0);
	iResult = send(mConnectSocket, messageBuffer->GetBufferData() + 8, totalLength, 0);
	if (iResult == SOCKET_ERROR) {
		printf("socket() failed with error: %d\n", iResult);
		closesocket(mConnectSocket);
		WSACleanup();
		connected = false;
		std::cout << "Lost connection to server" << std::endl;
	}
	DeleteRoomName(roomName);
	std::cout << "You have just left the room: " << roomName << std::endl;
}

//Name: SendMessageToServer
//Purpose: to send a string message to the server
//Accepts: a string representing the room name that the client wants to leave,
//         and the message itself
//Returns: nothing
void TCPClient::SendMessageToServer(std::string message, std::string roomName)
{
	if (connected)
	{
		try
		{
			//prefix the message with out username before sending it out
			message = username + ":" + message;

			messageBuffer->SetWriteIndex(0);
			//serialize the total length
			int totalLength = 8 + message.size() + roomName.size();
			messageBuffer->WriteInt32BE(totalLength);
			//serialize the message id
			messageBuffer->WriteInt32BE(MessageType::MESSAGE);

			//serialize room name length
			messageBuffer->WriteInt32BE(roomName.size());
			//serialize room name
			messageBuffer->WriteString(roomName);
			//serialize the message length
			messageBuffer->WriteInt32BE(message.size());
			//serialize the message
			messageBuffer->WriteString(message);

			//actually sending the data
			iResult = send(mConnectSocket, messageBuffer->GetBufferData(), sizeof(uint32_t) * 2, 0);
			iResult = send(mConnectSocket, messageBuffer->GetBufferData() + 8, totalLength, 0);
			if (iResult == SOCKET_ERROR) {
				closesocket(mConnectSocket);
				WSACleanup();
				connected = false;
			}
			if (iResult == 0) {
				closesocket(mConnectSocket);
				WSACleanup();
				connected = false;
			}
		}
		catch (...)
		{
			//Catch any exceptions that might come up to multithreading woes
		}
	}
}

//Name: ListenToServer
//Purpose: to receive messages from the server
//Accepts: nothing
//Returns: nothing
void TCPClient::ListenToServer()
{
	do {
		//take in the header //getting packet length and message id
		iResult = recv(mConnectSocket, messageBuffer->GetBufferData(), sizeof(uint32_t) * 2, 0);

		if (iResult == SOCKET_ERROR) {
			printf("recv failed with error: %d\n", WSAGetLastError());
			closesocket(mConnectSocket);
			WSACleanup();
			connected = false;
			std::cout << "Lost connection to server" << std::endl;
		}

		if (iResult == 0) {
			closesocket(mConnectSocket);
			WSACleanup();
			connected = false;
			std::cout << "Lost connection to server" << std::endl;
		}

		if (connected) 
		{
			messageBuffer->SetReadIndex(0);

			int packetLength = messageBuffer->ReadInt32BE();
			int message_id = messageBuffer->ReadInt32BE();

			//if incoming packet is larger than buffer, increase buffer size
			if (packetLength > messageBuffer->GetBufferSize()) {
				messageBuffer->ResizeBuffer(packetLength);
			}

			switch (message_id)
			{
			case MessageType::MESSAGE:
				//might have to do another recv here using the packetLength
				iResult = recv(mConnectSocket, messageBuffer->GetBufferData(), packetLength, 0);
				if (iResult == SOCKET_ERROR) {
					printf("recv failed with error: %d\n", WSAGetLastError());
					closesocket(mConnectSocket);
					WSACleanup();
					connected = false;
					std::cout << "Lost connection to server" << std::endl;
				}

				if (iResult == 0) {
					closesocket(mConnectSocket);
					WSACleanup();
					connected = false;
					std::cout << "Lost connection to server" << std::endl;
				}
				if (connected)
				{
					messageBuffer->SetReadIndex(0);
					//get the room name
					int roomNameLength = messageBuffer->ReadInt32BE();
					std::string roomName = messageBuffer->ReadString(roomNameLength);

					//get the message
					int messageLength = messageBuffer->ReadInt32BE();
					std::string message = messageBuffer->ReadString(messageLength);
					std::cout << message << std::endl;
				}

				break;
			}
		}

	} while (iResult > 0 && connected);
}

//Name: InsertRoomName
//Purpose: to add a room name to the local copy of what rooms the client is in
//Accepts: the name of the room to join
//Returns: nothing
void TCPClient::InsertRoomName(std::string roomName)
{
	roomNames.push_back(roomName);
}

//Name: DeleteRoomName
//Purpose: to remove a room name from the local copy of what rooms the client is in
//Accepts: the name of the room to leave
//Returns: nothing
void TCPClient::DeleteRoomName(std::string roomName)
{
	roomNames.erase(std::remove(roomNames.begin(), roomNames.end(), roomName), roomNames.end());
	if (roomNameIndex >= roomNames.size())
	{
		roomNameIndex = 0;
	}
}
