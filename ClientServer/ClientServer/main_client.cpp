#include "TCPClient.h"
#include <thread>
#include <regex>
#include <algorithm> 
#include <cctype>
#include <locale>
#include "Utils.h"
#include <sstream>

using namespace std;


int main(int argc, char** argv)
{

	TCPClient client;
	thread listenThread;

	//Prompt the user for their username and room name
	cout << "Please enter your username" << endl;
	getline(cin, client.username);
	string message = "continue";
	cout << "Thank you, now enter the room name of the room you wish to join" << endl;
	cout << "If the room does not exist, then one will be created for you" << endl;

	string roomName;
	getline(cin, roomName);

	//attempt to connect to the server
	client.ConnectToServer();

	if (client.connected) {
		listenThread = thread(&TCPClient::ListenToServer, client);
		cout << "Please input a message to send to the server" << endl;

		client.JoinRoom(roomName);

		regex leaveRegex("leave room");
		regex enterRegex("join room");

		while (client.connected)
		{
			getline(cin, message);
			if (client.connected)
			{
				if (regex_search(message, leaveRegex))
				{
					string extractedMessage = message.substr(10, message.size() - 10);
					trim(extractedMessage);
					client.LeaveRoom(extractedMessage);
				}
				else if (regex_search(message, enterRegex))
				{
					string extractedMessage = message.substr(9, message.size() - 9);
					trim(extractedMessage);
					client.JoinRoom(extractedMessage);
				}
				else if (message == "display rooms")
				{
					if (client.roomNames.size() > 1)
					{
						cout << "[Press the corresponding number to make a room the current room]" << endl;
						int size = client.roomNames.size();
						for (int roomIndex = 0; roomIndex < size; roomIndex++)
						{
							cout << "[" << roomIndex << "]" << client.roomNames[roomIndex] << " ";
						}
						cout << endl;
						string response;
						cin >> response;
						stringstream conversionStream;
						conversionStream.clear();
						conversionStream.str("");
						conversionStream.str(response);
						int responseNumber = 0;
						conversionStream >> responseNumber;
						if ((unsigned)responseNumber < client.roomNames.size())
						{
							client.roomNameIndex = responseNumber;
							cout << "Current room is now: " << client.roomNames[client.roomNameIndex] << endl;
						}
						else
						{
							cout << "Not valid input: " << endl;
						}

					}
					else if (client.roomNames.size() == 1)
					{
						cout << "Only one room currently: " << client.roomNames.front() << endl;
					}
					else
					{
						cout << "You are in zero rooms currently " << endl;
					}

				}
				else
				{
					if ((unsigned)client.roomNameIndex < client.roomNames.size())
					{
						//Actually sending the message to the server
						client.SendMessageToServer(message, client.roomNames[client.roomNameIndex]);
					}
					else
					{
						cout << "Could not send message because the current room does not exist" << endl;
					}
				}

			}
		}

		//wait for the listen thread to stop
		listenThread.join();
	}
	else
	{
		cout << "Failed to connect to server." << endl;
	}
	system("pause");
	exit(0);
	return 0;
}